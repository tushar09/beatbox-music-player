package beat.box.music.player;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class About extends ActionBarActivity {

    Tracker t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("About");
        t.send(new HitBuilders.AppViewBuilder().build());
        TextView tv = (TextView) findViewById(R.id.textView65);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            tv.setText("Version: " + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            tv.setText("Version: *.*.*.*");
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(About.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(About.this).reportActivityStart(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
