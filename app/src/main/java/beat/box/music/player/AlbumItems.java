package beat.box.music.player;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.FileDescriptor;
import java.util.ArrayList;


public class AlbumItems extends ActionBarActivity {

    public ArrayList fullAlbum = new ArrayList();
    public static SharedPreferences sp;
    private static MainActivity m = new MainActivity();
    private static ListView albumItems;
    public ArrayList aTitle = new ArrayList();
    private ArrayList aArtist = new ArrayList();
    private ArrayList aSongsPath = new ArrayList();
    private ArrayList aDuration = new ArrayList();
    private ArrayList<Long> aAlbumId = new ArrayList<Long>();
    private static Context context;
    Tracker t;
    AdView av;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_items);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("AlbumItems");
        t.send(new HitBuilders.AppViewBuilder().build());

        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        RelativeLayout albumBase = (RelativeLayout) findViewById(R.id.albumBase);
        albumItems = (ListView) findViewById(R.id.albumItems);
        context = this;
        Bundle b = getIntent().getExtras();

        String albumName = b.getString("albumName");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setTitle(albumName);
        for(int t = 0; t < m.albumName.size(); t++){
            if(m.albumName.get(t).equals(albumName)){
                aTitle.add(m.title.get(t));
                aArtist.add(m.artist.get(t));
                aDuration.add(m.songsDuration.get(t));
                aSongsPath.add(m.songsPath.get(t));
                aAlbumId.add(m.albumId.get(t));
            }
        }
        //Drawable d = new BitmapDrawable(bitmap);

        if(Build.VERSION.SDK_INT > 10){
            albumBase.setBackground(new BitmapDrawable(context.getResources(), getBitMap(aAlbumId.get(0))));
        }

        albumItems.setAdapter(new CustomListViewSongsForAlbumItems(context, aTitle, aArtist, aDuration, aSongsPath, aAlbumId));
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(AlbumItems.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
        GoogleAnalytics.getInstance(AlbumItems.this).reportActivityStart(this);
    }

    private Bitmap getBitMap(Long album_id) {
        Log.e("album id", "" + album_id);
        Bitmap bm = null;
        try {
            final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
            ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "r");
            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                bm = BitmapFactory.decodeFileDescriptor(fd);
            } else {
                bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.picture);
            }

        } catch (Exception e) {

        }
        if(bm == null){
            bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.picture);
        }else {

        }
        bm = toGrayscale(bm);
        return bm;
    }
    public Bitmap toGrayscale(Bitmap bmpOriginal){
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
