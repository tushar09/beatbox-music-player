package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Collections;


public class ArtistAlbum extends ActionBarActivity {

    public static SharedPreferences sp;

    private static Context context;
    private static GridView artistAlbumGridView;
    private static MainActivity f = new MainActivity();

    private ArrayList artistAlbum = new ArrayList();
    private ArrayList artistAlbumNonDup = new ArrayList();
    private ArrayList artistalTotalItems = new ArrayList();
    private ArrayList artistTitle = new ArrayList();
    private ArrayList artistAlbumId = new ArrayList();
    Tracker t;
    AdView av;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_album);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("ArtistAlbum");
        t.send(new HitBuilders.AppViewBuilder().build());

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        actionBar.setDisplayHomeAsUpEnabled(true);
        artistAlbumGridView = (GridView) findViewById(R.id.artistAlbumGridView);

        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);


        context = this;
        Bundle bundle = getIntent().getExtras();
        String artistTitle = bundle.getString("artistTitle");

        for(int t = 0; t < f.artist.size(); t++){
            String s = f.artist.get(t);
            if(artistTitle.equals(s)){
                artistAlbum.add(f.albumName.get(t));
            }
        }
        Collections.sort(artistAlbum, String.CASE_INSENSITIVE_ORDER);
        for(int t = 0; t < artistAlbum.size(); t++){
            if(!artistAlbumNonDup.contains(artistAlbum.get(t))){
                String s = "" + artistAlbum.get(t);
                artistAlbumNonDup.add(s);
                artistalTotalItems.add(f.albumNameTotalItem.get(f.albumNameSorted.indexOf(s)));
                artistAlbumId.add(f.albumNamesotedId.get(f.albumNameSorted.indexOf(s)));
                this.artistTitle.add(artistTitle);
            }

        }
        artistAlbumGridView.setAdapter(new CustomListViewAlbumFromArtist(context, artistAlbumNonDup, artistAlbumId, this.artistTitle, artistalTotalItems));
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(ArtistAlbum.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
        GoogleAnalytics.getInstance(ArtistAlbum.this).reportActivityStart(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
