package beat.box.music.player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

public class CallHandeler extends BroadcastReceiver {

    public static EgineBackground eg = new EgineBackground();
    public static MainActivity ma = new MainActivity();

    public CallHandeler() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (state == null) {
            if (eg.mp != null) {
                if (eg.mp.isPlaying()) {
                    if (ma.cb.isChecked()) {
                        eg.mp.pause();


                    }

                }
            }
        } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)
                || state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)
                || state.equals(TelephonyManager.EXTRA_STATE_RINGING))
        {
            if (eg.mp != null) {
                if (eg.mp.isPlaying()) {
                    if (ma.cb.isChecked()) {


                        eg.mp.pause();
                    }
                }
            }
        }
    }
}
