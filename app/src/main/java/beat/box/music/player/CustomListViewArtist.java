package beat.box.music.player;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tushar on 2/27/2015.
 */
public class CustomListViewArtist extends BaseAdapter {

    public static final String qryTotalPlayTime = "totalPlayTime";
    public static final String qrySongname = "songName";
    public static final String qryCurrentPosition = "currentPosition";


    public static final String DEFAULT_SONGNAME = "N/A";
    public static final long DEFAULT_TOTALPLAYTIME = 0;
    public static final long DEFAULT_CURRENTPOSITION = 0;


    private Context context;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private boolean gone;

    private static MainActivity f = new MainActivity();
    private ArrayList artistTitle, totalItems;


    private static LayoutInflater inflater = null;

    public CustomListViewArtist(Context context, ArrayList artistTitle, ArrayList totalItems){

        this.context = context;
        this.artistTitle = artistTitle;
        this.totalItems = totalItems;
        sp = this.context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
    }
    public CustomListViewArtist(Context context, ArrayList artistTitle, ArrayList totalItems, boolean gone){
        this.gone = gone;
        this.context = context;
        this.artistTitle = artistTitle;
        this.totalItems = totalItems;
        sp = this.context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    @Override
    public int getCount() {
        return artistTitle.size();
    }

    @Override
    public Object getItem(int position) {
        return artistTitle.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;

        if(convertView == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customlistviewartistssinglerow, null);
            holder = new Holder();
            holder.artistTitle = (TextView) convertView.findViewById(R.id.songTitle);
            holder.totalItems = (TextView) convertView.findViewById(R.id.songTitleArtist);

            convertView.setTag(holder);
        }else{
            Log.i("TAG", "inside");
            holder = (Holder) convertView.getTag();
        }
        holder.artistTitle.setText("" + artistTitle.get(position));
        holder.totalItems.setText("" + totalItems.get(position) + " albums");

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent artistAlbum = new Intent(context, ArtistAlbum.class);
                artistAlbum.putExtra("artistTitle", "" + artistTitle.get(position));
                context.startActivity(artistAlbum);
            }
        });

        return convertView;
    }


    public static class Holder {
        TextView artistTitle, totalItems;
        ImageButton menu;
        int position;

    }




    public void filte(){
        Log.e("safd", "asdf");
    }


}
