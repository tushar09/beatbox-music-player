package beat.box.music.player;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tushar on 2/10/2015.
 */
public class CustomListViewDataBase extends BaseAdapter {

    private static Uri uri;
    private static final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
    ArrayList songName, songFrequency, songPlayedLastTime;
    Context context;

    private static LayoutInflater inflater = null;

    public CustomListViewDataBase(Context mainActivity, ArrayList songName, ArrayList songFrequency, ArrayList songPlayedLastTime) {

        this.songName = songName;
        this.songFrequency = songFrequency;
        this.songPlayedLastTime = songPlayedLastTime;

        context = mainActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return songName.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    Holder holder;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        if(rowView == null){
            rowView = inflater.inflate(R.layout.customlistviewalbumsinglerow, null);
            holder = new Holder();
            holder.tv = (TextView) rowView.findViewById(R.id.title);
            holder.artist = (TextView) rowView.findViewById(R.id.textView55);
            holder.totalItem = (TextView) rowView.findViewById(R.id.textView56);
            holder.img = (TextView) rowView.findViewById(R.id.albumCover);
            rowView.setTag(holder);
        }else{
            holder = (Holder) rowView.getTag();
        }
        holder.tv.setText("" + songName.get(position));
        holder.totalItem.setText("Total Played: " + songFrequency.get(position));
        holder.artist.setText("" + songPlayedLastTime.get(position));
        holder.img.setText("" + songFrequency.get(position));
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(context, AlbumItems.class);
//                i.putExtra("albumName", "" + albumName.get(position));
//                context.startActivity(i);
//                Toast.makeText(context, "You Clicked " + albumName.get(position), Toast.LENGTH_LONG).show();
            }
        });

        return rowView;
    }

    public class Holder {
        TextView tv, artist, totalItem;
        TextView img;
        int position;
    }

}
