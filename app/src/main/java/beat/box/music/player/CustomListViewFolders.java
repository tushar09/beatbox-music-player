package beat.box.music.player;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tushar on 2/27/2015.
 */
public class CustomListViewFolders extends BaseAdapter {

    public static final String qryTotalPlayTime = "totalPlayTime";
    public static final String qrySongname = "songName";
    public static final String qryCurrentPosition = "currentPosition";


    public ArrayList folder = new ArrayList();

    public static final String DEFAULT_SONGNAME = "N/A";
    public static final long DEFAULT_TOTALPLAYTIME = 0;
    public static final long DEFAULT_CURRENTPOSITION = 0;


    private Context context;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private static MainActivity f = new MainActivity();
    private ArrayList songs, artist, songduration;


    private static LayoutInflater inflater = null;

    public CustomListViewFolders(Context context, ArrayList songs) {
        this.context = context;
        this.songs = songs;

        sp = this.context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.initilize_storage, null);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(songs.get(position).equals("Phone Memory")){
                    Intent i = new Intent(context, PhoneMemory.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    context.startActivity(i);
                }
                if(songs.get(position).equals("Micro SD Card")){
                    Intent i = new Intent(context, CardMemory.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    context.startActivity(i);
                }
            }
        });

        TextView tv = (TextView) convertView.findViewById(R.id.textView23);
        tv.setText("" + songs.get(position));
        return convertView;

    }
}

