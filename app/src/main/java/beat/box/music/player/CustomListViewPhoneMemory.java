package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tushar on 2/27/2015.
 */
public class CustomListViewPhoneMemory extends BaseAdapter {

    public static final String qryTotalPlayTime = "totalPlayTime";
    public static final String qrySongname = "songName";
    public static final String qryCurrentPosition = "currentPosition";


    public ArrayList folder = new ArrayList();

    public static final String DEFAULT_SONGNAME = "N/A";
    public static final long DEFAULT_TOTALPLAYTIME = 0;
    public static final long DEFAULT_CURRENTPOSITION = 0;


    private Context context;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private static MainActivity f = new MainActivity();
    private List songs, artist, songduration;


    private static LayoutInflater inflater = null;

    public CustomListViewPhoneMemory(Context context, List<String> songs) {
        this.context = context;
        this.songs = songs;

        sp = this.context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.storage_files, null);
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

        TextView tv = (TextView) convertView.findViewById(R.id.textView23);
        ImageView iv = (ImageView) convertView.findViewById(R.id.imageView7);
        tv.setText("" + songs.get(position));
        //File f = new File(songs.get(position).toString());
        if(position == 0 && !songs.get(position).toString().endsWith("/")){
            iv.setBackgroundResource(R.drawable.storage);
        }else if(songs.get(position).toString().endsWith(".MP3") || songs.get(position).toString().endsWith(".mp3") || songs.get(position).toString().endsWith(".wav") || songs.get(position).toString().endsWith(".WAV") || songs.get(position).toString().endsWith(".AMR") || songs.get(position).toString().endsWith(".amr")){
            iv.setBackgroundResource(R.drawable.music);
        }else if(songs.get(position).toString().endsWith("/")){

        }else if(songs.get(position).toString().endsWith("back")){
            iv.setBackgroundResource(R.drawable.back);
        }else{
            iv.setBackgroundResource(R.drawable.file);
        }
        return convertView;

    }
}

