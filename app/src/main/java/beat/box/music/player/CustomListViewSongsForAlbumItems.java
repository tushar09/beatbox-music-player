package beat.box.music.player;

import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Tushar on 2/27/2015.
 */
public class CustomListViewSongsForAlbumItems extends BaseAdapter {

    public static final String qryTotalPlayTime = "totalPlayTime";
    public static final String qrySongname = "songName";
    public static final String qryCurrentPosition = "currentPosition";


    public static final String DEFAULT_SONGNAME = "N/A";
    public static final long DEFAULT_TOTALPLAYTIME = 0;
    public static final long DEFAULT_CURRENTPOSITION = 0;


    private Context context;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private static MainActivity f = new MainActivity();
    private static AlbumItems m = new AlbumItems();
    private static EgineBackground eg = new EgineBackground();
    private ArrayList songs, artist, songduration, songsPath, albumId;


    private static LayoutInflater inflater = null;

    public CustomListViewSongsForAlbumItems(Context context, ArrayList songs, ArrayList artist, ArrayList songDuration, ArrayList songsPath, ArrayList albumId){
        this.albumId = albumId;
        this.songsPath = songsPath;
        this.context = context;
        this.songs = songs;
        this.artist = artist;
        this.songduration = songDuration;
        sp = this.context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;

        //View rowView = convertView;
        if(convertView == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customlistviewsongssinglerowinner, null);
            holder = new Holder();
            holder.songTitle = (TextView) convertView.findViewById(R.id.songTitle);
            holder.songTitleArtist = (TextView) convertView.findViewById(R.id.songTitleArtist);
            holder.songDuration = (TextView) convertView.findViewById(R.id.songDuration);
            holder.menu = (ImageButton) convertView.findViewById(R.id.menu);
            convertView.setTag(holder);
        }else{
            Log.i("TAG", "inside");
            holder = (Holder) convertView.getTag();
            //rowView.setTag(holder);
        }
        holder.songTitle.setText("" + songs.get(position));
        holder.songTitleArtist.setText("" + artist.get(position));
        long duration = Long.parseLong("" + songduration.get(position));
        int seconds = (int) ((duration / 1000) % 60);
        long minutes = ((duration - seconds) / 1000) / 60;
        if (seconds < 10) {
            holder.songDuration.setText("" + minutes + ":0" + seconds);
        } else {

            holder.songDuration.setText("" + minutes + ":" + seconds);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                f.fullPathList = new ArrayList(songsPath);
                f.fullSongDurationList = new ArrayList(songduration);
                f.fullTitleList = new ArrayList(songs);
                f.fullArtistList = new ArrayList(artist);
                f.fullalbumIdList = new ArrayList<Long>(albumId);
                f.saveSongForWidget();

                int positionAlbumItems = f.title.indexOf(songs.get(position));
                Intent player = new Intent(context, Player.class);
                player.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                player.putExtra("canStart", true);

                
                editor.putString("uri", "" + f.songsPath.get(positionAlbumItems));
                editor.putString("from folder to player", "no");
                editor.putInt("songId", position);
                editor.commit();
//                palyer.putExtra("uri", "" + f.songsPath.get(position));
//                palyer.putExtra("position", position);
                context.startActivity(player);
                eg.setNotificationPlayerIcon();
                eg.updateNotification();


//                AppWidgetManager awm = AppWidgetManager.getInstance(context);
//                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
//                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
//                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
//                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
//                awm.updateAppWidget(thisWidget, rv);
                AppWidgetManager awm = AppWidgetManager.getInstance(context);
                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
                rv.setTextViewText(R.id.textView67, "" + f.fullArtistList.get(position));
                if(eg.notification != null){
                    Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                    Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eg.notification);
                    Picasso.with(context).load(getBitMapUri(f.fullalbumIdList.get(position))).into(rv, R.id.imageView11, 1, eg.notification);
                }

                awm.updateAppWidget(thisWidget, rv);
            }
        });

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu pm = new PopupMenu(context, v);
                MenuInflater mi = pm.getMenuInflater();
                mi.inflate(R.menu.album_items_details, pm.getMenu());
                pm.show();

                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.addtoplaylist) {

                            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View popupView = inflator.inflate(R.layout.activity_play_list, null);
                            Dialog dg = new Dialog(context);;
                            dg.setTitle("" + f.fullTitleList.get(position));
                            dg.setContentView(popupView);

                            dg.show();
                            ListView lv = (ListView) dg.findViewById(R.id.listView4);
                            ArrayList ary = new ArrayList();
                            File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                            if (!mydir.exists()) {
                                mydir.mkdir();
                            }
                            File directory = new File(mydir.toString());
                            File[] listPlaylist = directory.listFiles();
                            for (File file : listPlaylist) {
                                if (file.isFile()) {
                                    ary.add(file.getName());
                                } else {

                                }
                            }
                            //showAssets();

                            lv.setAdapter(new DialogPplaylistListviewAlbumItemsDetails(context, ary, position, getMusicInformation(position), "" + songs.get(position)));
                            Button addNew = (Button) dg.findViewById(R.id.button2);
                            final Dialog finalDg = dg;
                            addNew.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Dialog newPlaylsit = new Dialog(context);
                                    newPlaylsit.setContentView(R.layout.addnewplaylist);
                                    newPlaylsit.setTitle("Add New Playlist");
                                    finalDg.dismiss();


                                    newPlaylsit.show();
                                    ImageButton add = (ImageButton) newPlaylsit.findViewById(R.id.imageButton31);
                                    ImageButton cancel = (ImageButton) newPlaylsit.findViewById(R.id.imageButton30);
                                    final EditText et = (EditText) newPlaylsit.findViewById(R.id.editText2);
                                    add.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (et.getText().toString().equals("") || et.getText().toString().equals(null)) {
                                                Toast.makeText(context, "Empty name can not be added", Toast.LENGTH_LONG).show();
                                            } else {

                                                String listName = et.getText().toString();
                                                File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                                                if (!mydir.exists()) {
                                                    mydir.mkdir();
                                                }
                                                File f = new File(mydir, listName);

                                                try {
                                                    FileOutputStream fos = new FileOutputStream(f, true);
                                                    String s = getMusicInformation(position) + "\n";
                                                    fos.write(s.getBytes());
                                                    fos.close();
                                                } catch (FileNotFoundException e) {
                                                    e.printStackTrace();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                newPlaylsit.dismiss();

                                            }
                                        }
                                    });
                                    cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            newPlaylsit.dismiss();
                                            finalDg.show();
                                        }
                                    });
                                }
                            });

                        }else if(item.getItemId() == R.id.addToFavourite){
                            File mydir = context.getDir("Favourites", Context.MODE_PRIVATE);
                            if (!mydir.exists()) {
                                mydir.mkdir();
                            }
                            File f = new File(mydir, "Favourite");
                            try {
                                FileOutputStream fos = new FileOutputStream(f, true);
                                String s = getMusicInformation(position) + "\n";
                                fos.write(s.getBytes());
                                fos.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else if (item.getItemId() == R.id.share) {
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            ShareMusicFile smf = new ShareMusicFile();
                            String tit = "" + songs.get(position);
                            sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(smf.getMusicFilePath(tit))));
                            sendIntent.setType("*/*");
                            context.startActivity(Intent.createChooser(sendIntent, "Share Via"));
                            return true;
                        }
                        return false;
                    }
                });

            }
        });

        return convertView;
    }

    private Uri getBitMapUri(Long album_id) {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
        return uri;
    }

    private String getMusicInformation(int position) {
        return
                songsPath.get(position) + "  _ _ _ _  "
                        + songduration.get(position) + "  _ _ _ _  "
                        + songs.get(position) + "  _ _ _ _  "
                        + artist.get(position) + "  _ _ _ _  "
                        + albumId.get(position) + "  _ _ _ _  ";
    }

    public static class Holder {
        TextView songTitle, songTitleArtist, songDuration;
        ImageButton menu;
        int position;

    }




    public void filte(){
        Log.e("safd", "asdf");
    }


}
