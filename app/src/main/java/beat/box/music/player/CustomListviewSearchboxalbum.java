package beat.box.music.player;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tushar on 2/27/2015.
 */
public class CustomListviewSearchboxalbum extends BaseAdapter {
    final Dialog dg;
    static ListView lv = null;

    public SearchBox sb;


    private Context context;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private String highlight;

    private static MainActivity f = new MainActivity();
    private ArrayList songs, artist, songduration, album, path, albumid;


    private static LayoutInflater inflater = null;

    public CustomListviewSearchboxalbum(Context context, ArrayList album, String highlight) {
        sb = new SearchBox();
        dg = new Dialog(context);

        this.highlight = highlight;
        this.context = context;

        this.album = album;

        sp = this.context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    @Override
    public int getCount() {
        return album.size();
    }

    @Override
    public Object getItem(int position) {
        return album.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;

        //View rowView = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.searchboxlistviewalbum, null);
            holder = new Holder();
            holder.albumTitle = (TextView) convertView.findViewById(R.id.title);
            //holder.songTitleArtist = (TextView) convertView.findViewById(R.id.textView59);
            //holder.songDuration = (TextView) convertView.findViewById(R.id.textView58);
            //holder.songalbum = (TextView) convertView.findViewById(R.id.textView57);
            //holder.menu = (ImageButton) convertView.findViewById(R.id.imageButton33);
            //holder.songTitleArtist = (TextView) convertView.findViewById(R.id.songTitleArtist);
            //holder.songDuration = (TextView) convertView.findViewById(R.id.songDuration);
            //holder.menu = (ImageButton) convertView.findViewById(R.id.menu);
            convertView.setTag(holder);
        } else {
            //Log.i("TAG","inside");
            holder = (Holder) convertView.getTag();
            //rowView.setTag(holder);
        }
        String t = "" + album.get(position);
        t = t.replaceAll("(?i)" + highlight, "<font color=" + "'#" + sp.getString("color", "f34335") + "'>" + highlight + "</font>");
        holder.albumTitle.setText(Html.fromHtml(t));
        //holder.songTitleArtist.setText("" + artist.get(position));
        //holder.songalbum.setText("" + album.get(position));



        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ArrayList path = new Arraylist



//
//                editor.putString("uri", "" + path.get(position));
//                editor.putString("from folder to player", "no");
//                editor.putInt("songId", 0);
////
                Intent searchBox = new Intent(context, SearchAlbum.class);
                searchBox.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                searchBox.putExtra("albumName", "" + album.get(position));

//
//                Log.e("latest position checking", "" + position);
                //editor.commit();
                context.startActivity(searchBox);
//
//                AppWidgetManager awm = AppWidgetManager.getInstance(context);
//                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
//                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
//                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
//                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
//                awm.updateAppWidget(thisWidget, rv);
            }
        });

//        holder.menu.setOnClickListener(new View.OnClickListener() {
//            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//            @Override
//            public void onClick(View v) {
//                PopupMenu pm = new PopupMenu(context, v);
//                MenuInflater mi = pm.getMenuInflater();
//                mi.inflate(R.menu.menu_songlist, pm.getMenu());
//                pm.show();
//                //
//                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    @TargetApi(Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        if (item.getItemId() == R.id.addtoplaylist) {
//
//                            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                            View popupView = inflator.inflate(R.layout.activity_play_list, null);
//
//                            dg.setTitle("" + f.fullTitleList.get(position));
//                            dg.setContentView(popupView);
//
//                            dg.show();
//                            lv = (ListView) dg.findViewById(R.id.listView4);
//                            ArrayList ary = new ArrayList();
//                            File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
//                            if (!mydir.exists()) {
//                                mydir.mkdir();
//                            }
//                            File directory = new File(mydir.toString());
//                            File[] listPlaylist = directory.listFiles();
//                            for (File file : listPlaylist) {
//                                if (file.isFile()) {
//                                    ary.add(file.getName());
//                                } else {
//
//                                }
//                            }
//                            showAssets();
//
//                            lv.setAdapter(new DialogPplaylistListview(context, ary, position));
//                            Button addNew = (Button) dg.findViewById(R.id.button2);
//                            addNew.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    final Dialog newPlaylsit = new Dialog(context);
//                                    newPlaylsit.setContentView(R.layout.addnewplaylist);
//                                    newPlaylsit.setTitle("Add New Playlist");
//                                    dg.dismiss();
//
//
//                                    newPlaylsit.show();
//                                    ImageButton add = (ImageButton) newPlaylsit.findViewById(R.id.imageButton31);
//                                    ImageButton cancel = (ImageButton) newPlaylsit.findViewById(R.id.imageButton30);
//                                    final EditText et = (EditText) newPlaylsit.findViewById(R.id.editText2);
//                                    add.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            if (et.getText().toString().equals("") || et.getText().toString().equals(null)) {
//                                                Toast.makeText(context, "Empty name can not be added", Toast.LENGTH_LONG).show();
//                                            } else {
//
//                                                String listName = et.getText().toString();
//                                                File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
//                                                if (!mydir.exists()) {
//                                                    mydir.mkdir();
//                                                }
//                                                File f = new File(mydir, listName);
//
//                                                try {
//                                                    FileOutputStream fos = new FileOutputStream(f, true);
//                                                    String s = getMusicInformation(position) + "\n";
//                                                    fos.write(s.getBytes());
//                                                    fos.close();
//                                                } catch (FileNotFoundException e) {
//                                                    e.printStackTrace();
//                                                } catch (IOException e) {
//                                                    e.printStackTrace();
//                                                }
//                                                newPlaylsit.dismiss();
//
//                                            }
//                                        }
//                                    });
//                                    cancel.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            newPlaylsit.dismiss();
//                                            dg.show();
//                                        }
//                                    });
//                                }
//                            });
//
//                        }else if(item.getItemId() == R.id.details){
//                            Intent i = new Intent(context, SongDetails.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                            i.putExtra("songName", "" + f.songs.get(position));
//                            i.putExtra("albumid", f.albumId.get(position));
//                            i.putExtra("artist", "" + f.artist.get(position));
//                            i.putExtra("year", "" + f.year.get(position));
//                            i.putExtra("composer", "" + f.composer.get(position));
//                            i.putExtra("album", "" + f.albumName.get(position));
//                            context.startActivity(i);
//                        }
//                        //Toast.makeText(context, "asdfasdfasfd", Toast.LENGTH_SHORT).show();
//                        return false;
//                    }
//                });
//            }
//        });


        return convertView;
    }

    private String getMusicInformation(int position) {
        return
                f.fullPathList.get(position) + "  _ _ _ _  "
                + f.fullSongDurationList.get(position) + "  _ _ _ _  "
                + f.fullTitleList.get(position) + "  _ _ _ _  "
                + f.fullArtistList.get(position) + "  _ _ _ _  "
                + f.fullalbumIdList.get(position) + "  _ _ _ _  ";
    }

//    private void showAssets() {
//        BufferedReader reader = null;
//        try {
//            reader = new BufferedReader(new InputStreamReader(context.getAssets().open("test.txt")));
//            String mLine = reader.readLine();
//            while (mLine != null) {
//                Log.e("reading", mLine);
//                mLine = reader.readLine();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

//    @Override
//    public Object[] getSections() {
//        return sections;
//    }
//
//    @Override
//    public int getPositionForSection(int sectionIndex) {
//        return alphaIndexer.get(sections[sectionIndex]);
//    }
//
//    @Override
//    public int getSectionForPosition(int position) {
//        return 0;
//    }


    public static class Holder {
        TextView albumTitle, songTitleArtist, songDuration, songalbum;
        ImageButton menu;
        //int position;

    }


}
