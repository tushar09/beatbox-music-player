package beat.box.music.player;

import android.app.AlertDialog;
import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Tushar on 2/27/2015.
 */
public class CustomListviewSearchboxtitle extends BaseAdapter implements AlertDialog.OnClickListener{
    final Dialog dg;
    static ListView lv = null;

    public SearchBox sb;
    int pos;

    private Context context;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private String highlight;
    private static EgineBackground eg = new EgineBackground();

    private static MainActivity f = new MainActivity();
    private ArrayList songs, artist, songduration, album, path, albumid;


    private static LayoutInflater inflater = null;

    public CustomListviewSearchboxtitle(Context context, ArrayList songs, ArrayList album, ArrayList artist, ArrayList duration, ArrayList path, ArrayList albumId, String highlight) {
        sb = new SearchBox();
        dg = new Dialog(context);

        this.highlight = highlight;
        this.context = context;
        this.songs = songs;
        this.artist = artist;
        this.album = album;
        this.path = path;
        this.albumid = albumId;
        songduration = duration;
        sp = this.context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;

        //View rowView = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.searchboxlistviewtitle, null);
            holder = new Holder();
            holder.songTitle = (TextView) convertView.findViewById(R.id.title);
            holder.songTitleArtist = (TextView) convertView.findViewById(R.id.textView59);
            holder.songDuration = (TextView) convertView.findViewById(R.id.textView58);
            holder.songalbum = (TextView) convertView.findViewById(R.id.textView57);
            holder.menu = (ImageButton) convertView.findViewById(R.id.imageButton33);
            //holder.songTitleArtist = (TextView) convertView.findViewById(R.id.songTitleArtist);
            //holder.songDuration = (TextView) convertView.findViewById(R.id.songDuration);
            //holder.menu = (ImageButton) convertView.findViewById(R.id.menu);
            convertView.setTag(holder);
        } else {
            //Log.i("TAG","inside");
            holder = (Holder) convertView.getTag();
            //rowView.setTag(holder);
        }
        String t = "" + songs.get(position);
        t = t.replaceAll("(?i)" + highlight, "<font color=" + "'#" + sp.getString("color", "f34335") + "'>" + highlight + "</font>");
        holder.songTitle.setText(Html.fromHtml(t));
        holder.songTitleArtist.setText("" + artist.get(position));
        holder.songalbum.setText("" + album.get(position));

        final long duration = Long.parseLong("" + songduration.get(position));
        int seconds = (int) ((duration / 1000) % 60);
        long minutes = ((duration - seconds) / 1000) / 60;
        if (seconds < 10) {
            holder.songDuration.setText("" + minutes + ":0" + seconds);
        } else {

            holder.songDuration.setText("" + minutes + ":" + seconds);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                //ArrayList path = new Arraylist

                f.fullPathList = new ArrayList();
                f.fullSongDurationList = new ArrayList();
                f.fullTitleList = new ArrayList();
                f.fullArtistList = new ArrayList();
                f.fullalbumIdList = new ArrayList();

                f.fullPathList.add(path.get(position));
                f.fullSongDurationList.add(songduration.get(position));
                f.fullTitleList.add(songs.get(position));
                f.fullArtistList.add(artist.get(position));
                f.fullalbumIdList.add(Long.parseLong("" + albumid.get(position)));
                f.saveSongForWidget();


//
                editor.putString("uri", "" + path.get(position));
                editor.putString("from folder to player", "no");
                editor.putInt("songId", 0);
//
                Intent palyer = new Intent(context, Player.class);
                palyer.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                palyer.putExtra("canStart", true);

                AppWidgetManager awm = AppWidgetManager.getInstance(context);
                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
                rv.setTextViewText(R.id.textView67, "" + f.fullArtistList.get(position));

                if(eg.notification != null){
                    Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                    Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eg.notification);
                    Picasso.with(context).load(getBitMapUri(f.fullalbumIdList.get(position))).into(rv, R.id.imageView11, 1, eg.notification);
                }

                awm.updateAppWidget(thisWidget, rv);

//
//                Log.e("latest position checking", "" + position);
                editor.commit();
                context.startActivity(palyer);
//
//                AppWidgetManager awm = AppWidgetManager.getInstance(context);
//                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
//                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
//                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
//                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
//                awm.updateAppWidget(thisWidget, rv);
            }
        });

//        holder.menu.setOnClickListener(new View.OnClickListener() {
//            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//            @Override
//            public void onClick(View v) {
//                PopupMenu pm = new PopupMenu(context, v);
//                MenuInflater mi = pm.getMenuInflater();
//                mi.inflate(R.menu.menu_songlist, pm.getMenu());
//                pm.show();
//                //
//                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    @TargetApi(Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        if (item.getItemId() == R.id.addtoplaylist) {
//
//                            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                            View popupView = inflator.inflate(R.layout.activity_play_list, null);
//
//                            dg.setTitle("" + f.fullTitleList.get(position));
//                            dg.setContentView(popupView);
//
//                            dg.show();
//                            lv = (ListView) dg.findViewById(R.id.listView4);
//                            ArrayList ary = new ArrayList();
//                            File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
//                            if (!mydir.exists()) {
//                                mydir.mkdir();
//                            }
//                            File directory = new File(mydir.toString());
//                            File[] listPlaylist = directory.listFiles();
//                            for (File file : listPlaylist) {
//                                if (file.isFile()) {
//                                    ary.add(file.getName());
//                                } else {
//
//                                }
//                            }
//                            showAssets();
//
//                            lv.setAdapter(new DialogPplaylistListview(context, ary, position));
//                            Button addNew = (Button) dg.findViewById(R.id.button2);
//                            addNew.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    final Dialog newPlaylsit = new Dialog(context);
//                                    newPlaylsit.setContentView(R.layout.addnewplaylist);
//                                    newPlaylsit.setTitle("Add New Playlist");
//                                    dg.dismiss();
//
//
//                                    newPlaylsit.show();
//                                    ImageButton add = (ImageButton) newPlaylsit.findViewById(R.id.imageButton31);
//                                    ImageButton cancel = (ImageButton) newPlaylsit.findViewById(R.id.imageButton30);
//                                    final EditText et = (EditText) newPlaylsit.findViewById(R.id.editText2);
//                                    add.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            if (et.getText().toString().equals("") || et.getText().toString().equals(null)) {
//                                                Toast.makeText(context, "Empty name can not be added", Toast.LENGTH_LONG).show();
//                                            } else {
//
//                                                String listName = et.getText().toString();
//                                                File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
//                                                if (!mydir.exists()) {
//                                                    mydir.mkdir();
//                                                }
//                                                File f = new File(mydir, listName);
//
//                                                try {
//                                                    FileOutputStream fos = new FileOutputStream(f, true);
//                                                    String s = getMusicInformation(position) + "\n";
//                                                    fos.write(s.getBytes());
//                                                    fos.close();
//                                                } catch (FileNotFoundException e) {
//                                                    e.printStackTrace();
//                                                } catch (IOException e) {
//                                                    e.printStackTrace();
//                                                }
//                                                newPlaylsit.dismiss();
//
//                                            }
//                                        }
//                                    });
//                                    cancel.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            newPlaylsit.dismiss();
//                                            dg.show();
//                                        }
//                                    });
//                                }
//                            });
//
//                        }else if(item.getItemId() == R.id.details){
//                            Intent i = new Intent(context, SongDetails.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                            i.putExtra("songName", "" + f.songs.get(position));
//                            i.putExtra("albumid", f.albumId.get(position));
//                            i.putExtra("artist", "" + f.artist.get(position));
//                            i.putExtra("year", "" + f.year.get(position));
//                            i.putExtra("composer", "" + f.composer.get(position));
//                            i.putExtra("album", "" + f.albumName.get(position));
//                            context.startActivity(i);
//                        }
//                        //Toast.makeText(context, "asdfasdfasfd", Toast.LENGTH_SHORT).show();
//                        return false;
//                    }
//                });
//            }
//        });
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                f.fullPathList = new ArrayList();
                f.fullSongDurationList = new ArrayList();
                f.fullTitleList = new ArrayList();
                f.fullArtistList = new ArrayList();
                f.fullalbumIdList = new ArrayList();

                f.fullPathList.add(path.get(position));
                f.fullSongDurationList.add(songduration.get(position));
                f.fullTitleList.add(songs.get(position));
                f.fullArtistList.add(artist.get(position));
                f.fullalbumIdList.add(Long.parseLong("" + albumid.get(position)));

                PopupMenu pm = new PopupMenu(context, v);
                MenuInflater mi = pm.getMenuInflater();
                mi.inflate(R.menu.menu_search_title, pm.getMenu());
                pm.show();
                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.addtoplaylist) {

                            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View popupView = inflator.inflate(R.layout.activity_play_list, null);

                            //dg.setTitle("" + f.fullTitleList.get(position));
                            dg.setTitle("" + songs.get(position));
                            dg.setContentView(popupView);

                            dg.show();
                            lv = (ListView) dg.findViewById(R.id.listView4);
                            ArrayList ary = new ArrayList();
                            File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                            if (!mydir.exists()) {
                                mydir.mkdir();
                            }
                            File directory = new File(mydir.toString());
                            File[] listPlaylist = directory.listFiles();
                            for (File file : listPlaylist) {
                                if (file.isFile()) {
                                    ary.add(file.getName());
                                } else {

                                }
                            }
                            //showAssets();

                            lv.setAdapter(new DialogPplaylistListview(context, ary, position));
                            Button addNew = (Button) dg.findViewById(R.id.button2);
                            addNew.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Dialog newPlaylsit = new Dialog(context);
                                    newPlaylsit.setContentView(R.layout.addnewplaylist);
                                    newPlaylsit.setTitle("Add New Playlist");
                                    dg.dismiss();


                                    newPlaylsit.show();
                                    ImageButton add = (ImageButton) newPlaylsit.findViewById(R.id.imageButton31);
                                    ImageButton cancel = (ImageButton) newPlaylsit.findViewById(R.id.imageButton30);
                                    final EditText et = (EditText) newPlaylsit.findViewById(R.id.editText2);
                                    add.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (et.getText().toString().equals("") || et.getText().toString().equals(null)) {
                                                Toast.makeText(context, "Empty name can not be added", Toast.LENGTH_LONG).show();
                                            } else {

                                                String listName = et.getText().toString();
                                                File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                                                if (!mydir.exists()) {
                                                    mydir.mkdir();
                                                }
                                                File f = new File(mydir, listName);

                                                try {
                                                    FileOutputStream fos = new FileOutputStream(f, true);
                                                    String s = getMusicInformation(position) + "\n";
                                                    fos.write(s.getBytes());
                                                    fos.close();
                                                } catch (FileNotFoundException e) {
                                                    e.printStackTrace();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                newPlaylsit.dismiss();

                                            }
                                        }
                                    });
                                    cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            newPlaylsit.dismiss();
                                            dg.show();
                                        }
                                    });
                                }
                            });

                        }else if (item.getItemId() == R.id.addtoqueue) {
                            File mydir = context.getDir("Favourites", Context.MODE_PRIVATE);
                            if (!mydir.exists()) {
                                mydir.mkdir();
                            }
                            File f = new File(mydir, "Favourite");
                            try {
                                FileOutputStream fos = new FileOutputStream(f, true);
                                String s = getMusicInformation(position) + "\n";
                                fos.write(s.getBytes());
                                fos.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else if(item.getItemId() == R.id.share){
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            ShareMusicFile smf = new ShareMusicFile();
                            String tit = "" + songs.get(position);
                            sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(smf.getMusicFilePath(tit))));
                            sendIntent.setType("*/*");
                            context.startActivity(Intent.createChooser(sendIntent, "Share Via"));
                        }else if(item.getItemId() == R.id.delete){
                            deleteSong("" + songs.get(position));
                        }
                        return true;
                    }
                });
            }
        });

        return convertView;
    }

    private void deleteSong(String s) {
        AlertDialog ad = new AlertDialog.Builder(context)
                .setMessage("Are You Sure?")
                .setIcon(R.drawable.ic_action_discard)
                .setTitle("Delete " + s + "?")
                .setPositiveButton("yes", this)
                .setNegativeButton("No", this)
                .setCancelable(false)
                .create();
        ad.show();
    }

    private Uri getBitMapUri(Long album_id) {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
        return uri;
    }

    private String getMusicInformation(int position) {
        return
                path.get(position) + "  _ _ _ _  "
                + songduration.get(position) + "  _ _ _ _  "
                + songs.get(position) + "  _ _ _ _  "
                + artist.get(position) + "  _ _ _ _  "
                + albumid.get(position) + "  _ _ _ _  ";
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE: // yes
                File file = new File("" + path.get(pos));
                boolean deleted = file.delete();
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                //f.populate(context);
                int i = f.songsPath.indexOf(path.get(pos));

                path.remove(pos); //remove item from inner path.
                songs.remove(pos);
                albumid.remove(pos);
                artist.remove(pos);
                songduration.remove(pos);

                f.title.remove(i);
                f.albumId.remove(i);
                f.albumName.remove(i);
                f.songs.remove(i);
                f.artist.remove(i);
                f.songsPath.remove(i);
                f.songsDuration.remove(i);
                f.year.remove(i);
                f.composer.remove(i);


                //ListView lv = (ListView) v.findViewById(R.id.listView);
                SearchAlbum sa = new SearchAlbum();
                sa.lv.setAdapter(new CustomListViewSongsAlbum(context, songs, artist, songduration, path, albumid));
                //f.songslist.setAdapter(new CustomListViewSongs(context, f.title, f.artist, f.songsDuration, f.songsPath));
                //f.adapter.notifyDataSetChanged();


                sa.lv.setSelection(pos);
                sa.lv.smoothScrollToPosition(pos);

                View v = f.pager.findViewWithTag("pos" + 0);
                ListView lva = (ListView) v.findViewById(R.id.listView);
                lva.setAdapter(new CustomListViewSongs(context, f.title, f.artist, f.songsDuration, f.songsPath));
//                f.populate(context);
//                Toast.makeText(context, "" + songspath.get(pos), Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(context, MainActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                context.startActivity(i);
//                f.finish();

                break;
            case DialogInterface.BUTTON_NEGATIVE: // no

                break;
            default:

                break;
        }
    }

//    private void showAssets() {
//        BufferedReader reader = null;
//        try {
//            reader = new BufferedReader(new InputStreamReader(context.getAssets().open("test.txt")));
//            String mLine = reader.readLine();
//            while (mLine != null) {
//                Log.e("reading", mLine);
//                mLine = reader.readLine();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

//    @Override
//    public Object[] getSections() {
//        return sections;
//    }
//
//    @Override
//    public int getPositionForSection(int sectionIndex) {
//        return alphaIndexer.get(sections[sectionIndex]);
//    }
//
//    @Override
//    public int getSectionForPosition(int position) {
//        return 0;
//    }


    public static class Holder {
        TextView songTitle, songTitleArtist, songDuration, songalbum;
        ImageButton menu;
        //int position;

    }


}
