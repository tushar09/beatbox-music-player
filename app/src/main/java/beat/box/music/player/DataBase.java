package beat.box.music.player;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Collections;

/*
 * Steps to using the DB:
 * 1. [DONE] Instantiate the DB Adapter
 * 2. [DONE] Open the DB
 * 3. [DONE] use get, insert, delete, .. to change data.
 * 4. [DONE]Close the DB
 */

/**
 * Demo application to show how to use the
 * built-in SQL lite database.
 */
public class DataBase extends ActionBarActivity implements AlertDialog.OnClickListener {

    private static boolean handlerChk = false;
    private static Handler handler;
    private static Runnable updateTask;
    private boolean bAnimation = true;
    private static TextView totalPlayBackTimer;
    private ArrayList songTitle = new ArrayList();
    private ArrayList songFrequency = new ArrayList();
    private ArrayList songPlayedLastTime = new ArrayList();
    private ListView lv;
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;

    DBAdapter myDb;
    Tracker t;
    AdView av;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("DataBase");
        t.send(new HitBuilders.AppViewBuilder().build());

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        actionBar.setDisplayHomeAsUpEnabled(true);
        lv = (ListView) findViewById(R.id.listView2);
        totalPlayBackTimer = (TextView) findViewById(R.id.totalPlaybackTimer);


        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        openDB();

        handler = new Handler();
        updateTask = new Runnable() {
            @Override
            public void run() {
                update();
                handler.postDelayed(this, 1000);
            }
        };
        if (!handlerChk) {
            handler.postDelayed(updateTask, 1000);
            handlerChk = true;
        }

        Cursor cursor = myDb.getAllRows();
        if (cursor.moveToFirst()) {
            do {
                // Process the data:
//                int id = cursor.getInt(DBAdapter.COL_ROWID);
//                String name = cursor.getString(DBAdapter.COL_NAME);
//                int studentNumber = cursor.getInt(DBAdapter.COL_STUDENTNUM);
//                String favColour = cursor.getString(DBAdapter.COL_FAVCOLOUR);
//
//                // Append data to the message:
//                message += "id=" + id
//                        +", name=" + name
//                        +", #=" + studentNumber
//                        +", Colour=" + favColour
//                        +"\n";
                songTitle.add(cursor.getString(DBAdapter.COL_NAME));
                songFrequency.add(cursor.getString(DBAdapter.COL_STUDENTNUM));
                songPlayedLastTime.add(cursor.getString(DBAdapter.COL_FAVCOLOUR));
            } while (cursor.moveToNext());
            cursor.close();
            lv.setAdapter(new CustomListViewDataBase(DataBase.this, songTitle, songFrequency, songPlayedLastTime));
        }

    }

    private void update() {
        if (totalPlayBackTimer != null) {
            long hours, minutes, seconds;
            long totalTime = sp.getLong("totalTime", 0);
            hours = totalTime / 3600;
            minutes = (totalTime % 3600) / 60;
            seconds = totalTime % 60;
            String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
            //totalTimeInDatabase.setText(timeString);

            if (bAnimation) {

                bAnimation = false;
                AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
                fadeIn.setDuration(2000);
                fadeIn.setFillAfter(true);
                totalPlayBackTimer.setText(timeString);
                totalPlayBackTimer.startAnimation(fadeIn);
            } else {
                totalPlayBackTimer.setText(timeString);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //closeDB();
    }


    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (!(activeNetworkInfo != null && activeNetworkInfo.isConnected())) {
            av.setVisibility(View.GONE);
        }
        bAnimation = true;
        handler.post(updateTask);
        super.onStart();
        GoogleAnalytics.getInstance(DataBase.this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(DataBase.this).reportActivityStop(this);
        handler.removeCallbacks(updateTask);
    }


    private void openDB() {
        myDb = new DBAdapter(this);
        myDb.open();
    }

    private void closeDB() {
        myDb.close();
    }


//	private void displayText(String message) {
//
//        Cursor cursor = myDb.getAllRows();
//        if (cursor.moveToFirst()) {
//            do {
//                // Process the data:
////                int id = cursor.getInt(DBAdapter.COL_ROWID);
////                String name = cursor.getString(DBAdapter.COL_NAME);
////                int studentNumber = cursor.getInt(DBAdapter.COL_STUDENTNUM);
////                String favColour = cursor.getString(DBAdapter.COL_FAVCOLOUR);
////
////                // Append data to the message:
////                message += "id=" + id
////                        +", name=" + name
////                        +", #=" + studentNumber
////                        +", Colour=" + favColour
////                        +"\n";
//                songTitle.add(cursor.getString(DBAdapter.COL_NAME));
//                songFrequency.add(cursor.getString(DBAdapter.COL_STUDENTNUM));
//                songPlayedLastTime.add(cursor.getString(DBAdapter.COL_FAVCOLOUR));
//            } while(cursor.moveToNext());
//            lv.setAdapter(new CustomListViewDataBase(DataBase.this, songTitle, songFrequency, songPlayedLastTime));
//        }
//	}


    public void onClick_AddRecord(View v) {
        //displayText("Clicked add record!");

        long newId = myDb.insertRow("Jenny", 5559, "Green");

        // Query for the record we just added.
        // Use the ID:
        Cursor cursor = myDb.getRow(newId);
        displayRecordSet(cursor);
    }

    public void onClick_ClearAll(View v) {
        //displayText("Clicked clear all!");
        myDb.deleteAll();
    }

    public void onClick_DisplayRecords(View v) {
        //displayText("Clicked display record!");

        Cursor cursor = myDb.getAllRows();
        displayRecordSet(cursor);
    }

    // Display an entire recordset to the screen.
    private void displayRecordSet(Cursor cursor) {
        String message = "";
        // populate the message from the cursor

        // Reset cursor to start, checking to see if there's data:
        if (cursor.moveToFirst()) {
            do {
                // Process the data:
                int id = cursor.getInt(DBAdapter.COL_ROWID);
                String name = cursor.getString(DBAdapter.COL_NAME);
                int studentNumber = cursor.getInt(DBAdapter.COL_STUDENTNUM);
                String favColour = cursor.getString(DBAdapter.COL_FAVCOLOUR);

                // Append data to the message:
                message += "id=" + id
                        + ", name=" + name
                        + ", #=" + studentNumber
                        + ", Colour=" + favColour
                        + "\n";
            } while (cursor.moveToNext());
        }

        // Close the cursor to avoid a resource leak.
        cursor.close();

        //displayText(message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_data_base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.sortByTitle) {
            sortByTitle();
        }
        if (id == R.id.sortByFrequency) {
            sortByFrequency();
        }
        if (id == R.id.clear) {
            clearDatabase();
        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    private void clearDatabase() {
        AlertDialog ad = new AlertDialog.Builder(DataBase.this)
                .setMessage("Clear The Database")
                .setIcon(R.drawable.ic_action_discard)
                .setTitle("Clear All Record?")
                .setPositiveButton("yes", this)
                .setNegativeButton("No", this)
                .setCancelable(false)
                .create();
        ad.show();
    }

    private void sortByFrequency() {
        for (int i = 0; i < songFrequency.size(); i++) {
            for (int j = 1; j < songFrequency.size(); j++) {
                if (Integer.parseInt("" + songFrequency.get(j - 1)) > Integer.parseInt("" + songFrequency.get(j))) {
                    int tempInt = Integer.parseInt("" + songFrequency.get(j));
                    String tempDate = "" + songPlayedLastTime.get(j);
                    String tempTitle = "" + songTitle.get(j);

                    songFrequency.set(j, songFrequency.get(j - 1));
                    songFrequency.set(j - 1, tempInt);

                    songPlayedLastTime.set(j, songPlayedLastTime.get(j - 1));
                    songPlayedLastTime.set(j - 1, tempDate);

                    songTitle.set(j, songTitle.get(j - 1));
                    songTitle.set(j - 1, tempTitle);


                }
            }
        }

        lv.setAdapter(new CustomListViewDataBase(DataBase.this, songTitle, songFrequency, songPlayedLastTime));

    }

    private void sortByTitle() {
        ArrayList tempfre = new ArrayList();
        ArrayList date = new ArrayList();
        ArrayList title = new ArrayList(songTitle);
        Collections.sort(title);
        for (int t = 0; t < title.size(); t++) {
            date.add(songPlayedLastTime.get(songTitle.indexOf(title.get(t))));
            tempfre.add(songFrequency.get(songTitle.indexOf(title.get(t))));
        }

        lv.setAdapter(new CustomListViewDataBase(DataBase.this, title, tempfre, date));
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                myDb.deleteAll();
                songFrequency = new ArrayList();
                songPlayedLastTime = new ArrayList();
                songTitle = new ArrayList();
                lv.setAdapter(new CustomListViewDataBase(DataBase.this, songTitle, songFrequency, songPlayedLastTime));
                break;

        }
    }
}










