package beat.box.music.player;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Tushar on 5/6/2015.
 */
public class DialogPplaylistListview extends BaseAdapter implements DialogInterface.OnClickListener {

    int position;
    ArrayList name;
    Context context;
    MainActivity f = new MainActivity();
    int parentPosition;
    boolean confirmDeletePlayList = false;

    DialogPplaylistListview(Context context, ArrayList name, int parentPosition) {
        this.context = context;
        this.name = name;
        this.parentPosition = parentPosition;
    }

    DialogPplaylistListview(Context context, ArrayList name) {
        this.context = context;
        this.name = name;
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        this.position = position;
        LayoutInflater i = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = i.inflate(R.layout.dialoglistviewplaylistsingleitems, null);
        TextView n = (TextView) v.findViewById(R.id.songTitle);
        n.setText("" + name.get(position));
        ImageButton deleteList = (ImageButton) v.findViewById(R.id.imageButton32);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                if (!mydir.exists()) {
                    mydir.mkdir();
                }
                File fs = new File(mydir, (String) name.get(position));
                try {
                    FileOutputStream fos = new FileOutputStream(fs, true);
                    String s = getMusicInformation(parentPosition) + "\n";
                    fos.write(s.getBytes());
                    fos.close();
//                    BufferedReader br = new BufferedReader(new FileReader(f));
//                    StringBuilder text = new StringBuilder();
//                    String line = "sdsfdgdsfgdsfg";
//                    while((line = br.readLine()) != null){
//                        text.append(line);
//                    }
//                    br.close();
                    Toast.makeText(context, "" + f.fullTitleList.get(parentPosition) + " is added to " + name.get(position), Toast.LENGTH_LONG).show();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        deleteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDelete((String) name.get(position));
                if (confirmDeletePlayList) {


                }
                //c.lv.setAdapter(new DialogPplaylistListview(context, ary));
            }
        });

        return v;
    }

    private void confirmDelete(String s) {
        AlertDialog ad = new AlertDialog.Builder(context)
                .setMessage("Are You Sure?")
                .setIcon(R.drawable.ic_action_discard)
                .setTitle("Delete " + s + "?")
                .setPositiveButton("yes", this)
                .setNegativeButton("No", this)
                .setCancelable(false)
                .create();
        ad.show();
    }

    private String getMusicInformation(int position) {
        return
                f.fullPathList.get(position) + "  _ _ _ _  "
                        + f.fullSongDurationList.get(position) + "  _ _ _ _  "
                        + f.fullTitleList.get(position) + "  _ _ _ _  "
                        + f.fullArtistList.get(position) + "  _ _ _ _  "
                        + f.fullalbumIdList.get(position) + "  _ _ _ _  ";
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE: // yes
                confirmDeletePlayList = true;
                File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                if (!mydir.exists()) {
                    mydir.mkdir();
                }
                File f = new File(mydir, (String) name.get(position));
                boolean deleted = f.delete();
                Toast.makeText(context, "" + deleted, Toast.LENGTH_SHORT);
                CustomListViewSongs c = new CustomListViewSongs(context, name, name, name, name);
                File directory = new File(mydir.toString());
                File[] listPlaylist = directory.listFiles();
                ArrayList ary = new ArrayList();
                for (File file : listPlaylist) {
                    if (file.isFile()) {
                        ary.add(file.getName());
                    } else {

                    }

                }
                c.lv.setAdapter(new DialogPplaylistListview(context, ary));
                //c.dg.show();
                break;
            case DialogInterface.BUTTON_NEGATIVE: // no
                confirmDeletePlayList = false;
                break;
            default:
                confirmDeletePlayList = false;
                break;
        }
    }
}
