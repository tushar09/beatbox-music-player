package beat.box.music.player;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.PresetReverb;
import android.media.audiofx.Virtualizer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.RemoteViews;

import com.squareup.picasso.Picasso;

import java.io.FileDescriptor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class EgineBackground extends Service implements MediaPlayer.OnCompletionListener {


    public static Equalizer eq;
    public static int[] totalBand;

    public float volume = 1f;
    private static int startCount = 0;
    public ArrayList fullSongList = new ArrayList();
    public static int localPosition;

    public static MainActivity m = new MainActivity();
    private static Uri uri;
    public int songId;
    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    TaskStackBuilder stackBuilder;
    PendingIntent pendingIntent;
    static DBAdapter db;
    public static RemoteViews remoteViews;
    static MediaPlayer mp = null;
    public static Notification notification;
    public static NotificationManager notificationManager;
    private CharSequence album;
    private Random random = new Random();
    private static Context context;
    public static BassBoost bb;
    public static Virtualizer vt;
    public static PresetReverb pr;
    private static Handler handler;
    private static Runnable updateTask;
    private static boolean handlerCheck = false;
    private boolean permissionNotify = false;

    private static Player p = new Player();

    @Override
    public void onCreate() {

        super.onCreate();
        context = this;
        db = new DBAdapter(this);
        db.open();
        stackBuilder = TaskStackBuilder.create(this);
        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();

        handler = new Handler();
        updateTask = new Runnable() {
            @Override
            public void run() {
                sleepMusic();
                //updateNotification();
                handler.postDelayed(this, 1000);
            }
        };
        if (!handlerCheck) {
            handler.postDelayed(updateTask, 1000);
            handlerCheck = true;
        }

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (mp == null) {
            Uri uri = Uri.parse("" + m.songsPath.get(0));
            mp = MediaPlayer.create(this, uri);
        }
        if (eq != null) {
            eq.release();
        }
        eq = new Equalizer(0, mp.getAudioSessionId());
        bb = new BassBoost(0, mp.getAudioSessionId());
        vt = new Virtualizer(0, mp.getAudioSessionId());
        pr = new PresetReverb(0, mp.getAudioSessionId());
        totalBand = new int[eq.getNumberOfBands()];
        //Log.e("total bands", "" + totalBand.length);
        startInForground();
        Log.e("Oncreate visited", "Visited");
        localPosition = sp.getInt("songId", 0);
    }

    private void sleepMusic() {
        if(mp != null){
            if(mp.isPlaying()){
                if(sp.getBoolean("sleepActivated", false)){
                    startCount++;
                    if((sp.getInt("sleepTime", 999999999) - startCount) < 30){
                        float devider = 30 - (sp.getInt("sleepTime", 999999999) - startCount);

                        volume -= 1f / 30.0f;
                        Log.e("dev", "" + volume);
                        mp.setVolume(volume, volume);

                    }
                    Log.e("startCount", "" + startCount);
                    if(startCount == sp.getInt("sleepTime", 999999999)){
                        mp.pause();
                        //mp.setVolume();
                        startCount = 0;
                        volume = 1.0f;
                        mp.setVolume(volume, volume);
                        editor.putBoolean("sleepActivated", false);
                        editor.commit();
                    }
                }
            }
        }

    }

    public void notificationColorChange() {
        if(notification != null){
            if(remoteViews != null){
                remoteViews.setImageViewResource(R.id.notbgd, sp.getInt("notBg", R.drawable.updatednotbg));
                notificationManager.notify(1, notification);
            }
        }

    }

    public void updateNotification() {
        notificationColorChange();
        if(notification != null){
            //if(permissionNotify){
            if(remoteViews != null){
                remoteViews.setTextViewText(R.id.textView3, getTitle());
                remoteViews.setTextViewText(R.id.textView4, getAlbum());
                remoteViews.setTextViewText(R.id.textView5, getArtist());
                    Bitmap bitmap = getBitMap(m.fullalbumIdList.get(sp.getInt("songId", 0)));
                    if (bitmap == null) {
                        Log.e("same", "no change from engine");
                        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.notificationicongeneral);
                    }
                remoteViews.setImageViewBitmap(R.id.imageView, bitmap);
                Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.notificationicongeneral);
                permissionNotify = false;
                Picasso.with(context).load(path).into(remoteViews, R.id.imageView, 1, notification);
                Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(remoteViews, R.id.imageView, 1, notification);
                //znotificationManager.notify(1, notification);
                //startInForground();
            }else{
                remoteViews = new RemoteViews(getPackageName(),
                        R.layout.notification_layout_big);
                Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.notificationicongeneral);
                permissionNotify = false;
                Picasso.with(context).load(path).into(remoteViews, R.id.imageView, 1, notification);
                Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(remoteViews, R.id.imageView, 1, notification);
            }
            //}
        }
        setNotificationPlayerIcon();
    }
    public void updateNotification2() {
        if(notification != null){
            //if(permissionNotify){
            if(remoteViews != null){
                remoteViews.setTextViewText(R.id.textView3, getTitle());
                remoteViews.setTextViewText(R.id.textView4, getAlbum());
                remoteViews.setTextViewText(R.id.textView5, getArtist());
//                    Bitmap bitmap = getBitMap(m.fullalbumIdList.get(sp.getInt("songId", 0)));
//                    if (bitmap == null) {
//                        Log.e("same", "no change from engine");
//                        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.notificationicongeneral);
//                    }
                //remoteViews.setImageViewBitmap(R.id.imageView, bitmap);
                Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.notificationicongeneral);
                permissionNotify = false;
                Picasso.with(context).load(path).into(remoteViews, R.id.imageView, 1, notification);
                Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(remoteViews, R.id.imageView, 1, notification);
                //znotificationManager.notify(1, notification);
                //startInForground();
            }
            //}
        }
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    void startInForground() {


        Intent myIntent = new Intent(this, Player.class);
        myIntent.putExtra("canStart", false);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        pendingIntent = PendingIntent.getActivity(this, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent pb = new Intent(this, PreviousButtonListener.class);
        PendingIntent previousButton = PendingIntent.getBroadcast(this, 0, pb, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent nb = new Intent(this, NextButtonListener.class);
        PendingIntent nextButton = PendingIntent.getBroadcast(this, 0, nb, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent plb = new Intent(this, PlayButtonListener.class);
        PendingIntent playButton = PendingIntent.getBroadcast(this, 0, plb, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent cb = new Intent(this, CancelButtonListener.class);
        PendingIntent cancellButton = PendingIntent.getBroadcast(this, 0, cb, PendingIntent.FLAG_UPDATE_CURRENT);


        //views.setOnClickPendingIntent(R.id.MY_BUTTON_ID, pendingIntent);


        //Log.e("Api version:", "" + Build.VERSION.SDK_INT);

        if (Build.VERSION.SDK_INT >= 16) {
            remoteViews = new RemoteViews(getPackageName(),
                    R.layout.notification_layout_big);
            notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.notificationiconappicon)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setPriority(128)
                    .build();

            notification.bigContentView = remoteViews;
            remoteViews.setTextViewText(R.id.textView3, getTitle());
            remoteViews.setTextViewText(R.id.textView4, getAlbum());
            remoteViews.setTextViewText(R.id.textView5, getArtist());
            remoteViews.setImageViewResource(R.id.notbgd, Color.parseColor("#" + sp.getString("color", "f34335")));
            remoteViews.setImageViewResource(R.id.imageButton, android.R.drawable.ic_media_previous);
            remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_pause);
            remoteViews.setImageViewResource(R.id.imageButton3, android.R.drawable.ic_media_next);
//            Bitmap bitmap = getBitMap(m.fullalbumIdList.get(sp.getInt("songId", 0)));
//            if (bitmap == null) {
//                Log.e("same", "no change from engine");
//                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.notificationicongeneral);
//            }
//            remoteViews.setImageViewBitmap(R.id.imageView, bitmap);

            Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(remoteViews, R.id.imageView, 1, notification);

            remoteViews.setOnClickPendingIntent(R.id.imageButton, previousButton);
            remoteViews.setOnClickPendingIntent(R.id.notPlay, playButton);
            remoteViews.setOnClickPendingIntent(R.id.imageButton3, nextButton);
            remoteViews.setOnClickPendingIntent(R.id.imageButton4, cancellButton);

        }
        startForeground(1, notification);

    }


    private static Bitmap getBitMap(Long album_id) {
        //Log.e("album id", "" + album_id);
        Bitmap bm = null;
        try {
            final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
            ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "r");
            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                bm = BitmapFactory.decodeFileDescriptor(fd);
            } else {
                bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.iconforgrid);
            }

        } catch (Exception e) {

        }
        return bm;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (uri != null) {
            mp.stop();
            mp.release();
            localPosition = sp.getInt("songId", 0);
            uri = Uri.parse("" + m.fullPathList.get(localPosition));
            Log.e("From service", "" + localPosition);
            mp = MediaPlayer.create(this, uri);
            if (eq != null) {
                eq.release();
            }
            if (bb != null) {
                bb.release();
            }
            if (vt != null) {
                vt.release();
            }
            if (pr != null) {
                pr.release();
            }
            eq = new Equalizer(0, mp.getAudioSessionId());
            bb = new BassBoost(0, mp.getAudioSessionId());
            vt = new Virtualizer(0, mp.getAudioSessionId());
            pr = new PresetReverb(0, mp.getAudioSessionId());

            if (sp.getBoolean("toggle", true)) {
                eq.setEnabled(true);
                bb.setEnabled(true);
                vt.setEnabled(true);
                pr.setEnabled(true);
            } else {
                eq.setEnabled(false);
                bb.setEnabled(false);
                vt.setEnabled(false);
                pr.setEnabled(false);
            }
            setUserPreset();
            if(sp.getBoolean("loadLastPositionChk", false)){
                mp.seekTo(sp.getInt("loadLastPosition", 0));
            }
            //intent.getStringExtra("fromPlayer").equals("fromPlayer");

            mp.start();
        }
        mp.setOnCompletionListener(this);
        return 1;
    }

    private void setUserPreset() {
        for (int t = 0; t < eq.getNumberOfBands(); t++) {
            String s = "band" + t;
            int frequency = sp.getInt(s, 0);
            eq.setBandLevel((short) t, (short) frequency);
        }
        bb.setStrength((short) sp.getInt("bb", 0));
        vt.setStrength((short) sp.getInt("vt", 0));
        pr.setPreset((short) sp.getInt("spinnerPosition2", 0));
        if (sp.getBoolean("toggle", true)) {
            eq.setEnabled(true);
            bb.setEnabled(true);
            vt.setEnabled(true);
            pr.setEnabled(true);
        } else {
            eq.setEnabled(false);
            bb.setEnabled(false);
            vt.setEnabled(false);
            pr.setEnabled(false);
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCompletion(MediaPlayer mp) {

        boolean haveToStop = false;

        int repeat = sp.getInt("repeat", 0);
        if (repeat == 0) {
            if (sp.getBoolean("suffel", false)) {
                //random = new Random(m.fullPathList.size());
                localPosition = random.nextInt(m.fullPathList.size());
                if (localPosition == m.fullPathList.size()) {
                    localPosition = 0;
                }
            } else {
                localPosition++;
                if (localPosition == m.fullPathList.size()) {
                    localPosition = 0;
                    haveToStop = true;
                }
            }
        } else if (repeat == 1) {
            localPosition = sp.getInt("songId", localPosition);
        } else if (repeat == 2) {
            if (sp.getBoolean("suffel", false)) {
                localPosition = random.nextInt(m.fullPathList.size());
                if (localPosition == m.fullPathList.size()) {
                    localPosition = 0;
                }
            } else {
                localPosition++;
                if (localPosition == m.fullPathList.size()) {
                    localPosition = 0;
                }
            }
        }

        mp.pause();

        editor.putInt("songId", localPosition);
        editor.commit();


        uri = Uri.parse("" + m.fullPathList.get(localPosition));
        mp = MediaPlayer.create(this, uri);
        this.mp = mp;
        try{
            this.mp.setOnCompletionListener(this);
        }catch (Exception e){

        }

        if (!haveToStop) {
            if (eq != null) {
                eq.release();
            }
            if (bb != null) {
                bb.release();
            }
            if (vt != null) {
                vt.release();
            }
            if (pr != null) {
                pr.release();
            }
            eq = new Equalizer(0, mp.getAudioSessionId());
            bb = new BassBoost(0, mp.getAudioSessionId());
            vt = new Virtualizer(0, mp.getAudioSessionId());
            pr = new PresetReverb(0, mp.getAudioSessionId());
            if (sp.getBoolean("toggle", true)) {
                eq.setEnabled(true);
                bb.setEnabled(true);
                vt.setEnabled(true);
                pr.setEnabled(true);
            } else {
                eq.setEnabled(false);
                bb.setEnabled(false);
                vt.setEnabled(false);
                pr.setEnabled(false);
            }
            setUserPreset();
            this.mp.start();
            String currentDate = DateFormat.getDateInstance().format(Calendar.getInstance().getTime());
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("HH:mm:ss");
            Date myDate = new Date();
            String currentDateTimeString = currentDate + "   (" + timeStampFormat.format(myDate) + ")";
            Cursor cursor = db.getAllRows();
            insert();
            cursor.close();
        }
        if(sp.getBoolean("isForegroud", false)){
            p.update2();
        }

        remoteViews.setTextViewText(R.id.textView3, getTitle());
        remoteViews.setTextViewText(R.id.textView4, getAlbum());
        remoteViews.setTextViewText(R.id.textView5, getArtist());
        updateNotification();

        AppWidgetManager awm = AppWidgetManager.getInstance(context);
        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
        ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
        rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
        rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(sp.getInt("songId", 0)));
        rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
        rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(sp.getInt("songId", 0)));
        rv.setTextViewText(R.id.textView67, "" + m.fullArtistList.get(sp.getInt("songId", 0)));
        if(notification != null){
            Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
            Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, notification);
            Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(rv, R.id.imageView11, 1, notification);
        }

        awm.updateAppWidget(thisWidget, rv);
    }

    public void updateNotificationFromAlbumItems(){
        remoteViews.setTextViewText(R.id.textView3, getTitle());
        remoteViews.setTextViewText(R.id.textView4, getAlbum());
        remoteViews.setTextViewText(R.id.textView5, getArtist());
        updateNotification();
    }

    void insert() {
        String currentDate = DateFormat.getDateInstance().format(Calendar.getInstance().getTime());
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("HH:mm:ss");
        Date myDate = new Date();
        String currentDateTimeString = currentDate + "   (" + timeStampFormat.format(myDate) + ")";
        Cursor cursor = db.getAllRows();
        if (cursor.moveToFirst()) {

            boolean found = false;
            do {
                if (cursor.getString(DBAdapter.COL_NAME).equals(m.fullTitleList.get(localPosition))) {
                    String name = cursor.getString(DBAdapter.COL_NAME);
                    int totalPlayed = cursor.getInt(DBAdapter.COL_STUDENTNUM);
                    int id = cursor.getInt(DBAdapter.COL_ROWID);
                    db.updateRow(id, cursor.getString(DBAdapter.COL_NAME), totalPlayed + 1, currentDateTimeString);
                    found = true;
                    break;
                } else {
                    //myDb.insertRow(songName, 1, "Green");
                }
            } while (cursor.moveToNext());
            if (!found) {
                db.insertRow("" + m.fullTitleList.get(localPosition), 1, currentDateTimeString);
            }
        } else {
            db.insertRow("" + m.fullTitleList.get(localPosition), 1, currentDateTimeString);
        }
        cursor.close();
    }

    void handleAllEcuStaff() {
        if (eq != null) {
            eq.release();
        }
        if (bb != null) {
            bb.release();
        }
        if (vt != null) {
            vt.release();
        }
        if (pr != null) {
            pr.release();
        }
        eq = new Equalizer(0, mp.getAudioSessionId());
        bb = new BassBoost(0, mp.getAudioSessionId());
        vt = new Virtualizer(0, mp.getAudioSessionId());
        pr = new PresetReverb(0, mp.getAudioSessionId());
        if (sp.getBoolean("toggle", true)) {
            eq.setEnabled(true);
            bb.setEnabled(true);
            vt.setEnabled(true);
            pr.setEnabled(true);
        } else {
            eq.setEnabled(false);
            bb.setEnabled(false);
            vt.setEnabled(false);
            pr.setEnabled(false);
        }
    }

    public void nextSong(String fromWhere) {
        localPosition = sp.getInt("songId", 0);

        if (mp.isPlaying()) {
            localPosition++;
            if (localPosition == m.fullPathList.size()) {
                localPosition = 0;
            }
            editor.putInt("songId", localPosition);
            editor.commit();
            mp.stop();
            mp.release();
            mp = null;
            this.uri = Uri.parse("" + m.fullPathList.get(localPosition));
            mp = MediaPlayer.create(this, this.uri);
            mp.setOnCompletionListener(this);
            handleAllEcuStaff();
            setUserPreset();
            if(fromWhere.equals("notificationBroadcast")){
                mp.start();
            }
            //mp.start();
            insert();
            //Log.e("From next ", "" + localPosition);
        } else {
            localPosition++;
            if (localPosition == m.fullPathList.size()) {
                localPosition = 0;
            }
            editor.putInt("songId", localPosition);
            editor.commit();
            setUserPreset();
            mp.stop();
            mp.release();
            mp = null;
            this.uri = Uri.parse("" + m.fullPathList.get(localPosition));
            mp = MediaPlayer.create(this, this.uri);
            mp.setOnCompletionListener(this);
            handleAllEcuStaff();
            setUserPreset();
            if(fromWhere.equals("notificationBroadcast")){
                mp.start();
            }
            insert();
            //Log.e("From next ", "" + songId);
        }

        permissionNotify = true;

        if(Build.VERSION.SDK_INT > 10){
            remoteViews.setTextViewText(R.id.textView3, getTitle());
            remoteViews.setTextViewText(R.id.textView4, getAlbum());
            remoteViews.setTextViewText(R.id.textView5, getArtist());
            updateNotification();
        }
        notificationColorChange();

        //notificationManager.notify(1, notification);
    }

    private Uri getBitMapUri(Long album_id) {
        //Log.e("album id", "" + album_id);
        //Bitmap bm = null;
        //try {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
        //ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "r");
        //if (pfd != null) {
        //FileDescriptor fd = pfd.getFileDescriptor();
        //bm = BitmapFactory.decodeFileDescriptor(fd);
        //} else {
        //bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.iconforgrid);
        //}

        //} catch (Exception e) {

        //}
        //return bm;
        return uri;
    }

    public void previousSong(String fromWhere) {
        localPosition = sp.getInt("songId", 0);
        if (mp.isPlaying()) {
            localPosition--;
            if (localPosition < 0) {
                localPosition = m.fullPathList.size() - 1;

            }
            editor.putInt("songId", localPosition);
            editor.commit();
            mp.stop();
            mp.release();
            this.uri = Uri.parse("" + m.fullPathList.get(localPosition));
            mp = MediaPlayer.create(this, this.uri);
            mp.setOnCompletionListener(this);
            handleAllEcuStaff();
            setUserPreset();
            if(fromWhere.equals("notificationBroadcast")){
                mp.start();
            }
            insert();
            //Log.e("From pre ", "" + songId);
        } else {
            localPosition--;
            if (localPosition < 0) {
                localPosition = m.fullPathList.size() - 1;
            }
            editor.putInt("songId", localPosition);
            editor.commit();
            mp.stop();
            mp.release();
            this.uri = Uri.parse("" + m.fullPathList.get(localPosition));
            mp = MediaPlayer.create(this, this.uri);
            mp.setOnCompletionListener(this);
            handleAllEcuStaff();
            setUserPreset();
            if(fromWhere.equals("notificationBroadcast")){
                mp.start();
            }

            insert();
            //Log.e("From pre ", "" + songId);
        }
        permissionNotify = true;

        if(Build.VERSION.SDK_INT > 10){
            remoteViews.setTextViewText(R.id.textView3, getTitle());
            remoteViews.setTextViewText(R.id.textView4, getAlbum());
            remoteViews.setTextViewText(R.id.textView5, getArtist());
            updateNotification();
        }
        notificationColorChange();

        //notificationManager.notify(1, notification);
    }

    public void setUri(String uri, int songId) {
        this.uri = Uri.parse(uri);
        //this.songId = songId;
        //Log.e("From setUri ", "" + this.songId);
    }


    @Override
    public void onDestroy() {
        editor.putInt("songId", songId);
        editor.commit();
        //db.close();
        super.onDestroy();
    }

    public CharSequence getTitle() {

        return "" + m.fullTitleList.get(songId = sp.getInt("songId", 0));
        //startInForground();
    }

    public void setNotificationPlayerIcon(){
        if(mp != null){
            if(!mp.isPlaying()){
                remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_play);
            }else {
                remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_pause);
            }
            notificationManager.notify(1, notification);
        }

    }

    public void setNotificationPlayerIcon2(){
        if(mp != null){
            if(!mp.isPlaying()){
                remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_pause);
            }else {
                remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_play);
            }
            notificationManager.notify(1, notification);
        }

    }

    public CharSequence getArtist() {
        songId = sp.getInt("songId", 0);
        if(songId >= m.fullTitleList.size()){
            return "" + m.fullArtistList.get(0);
        }else if(songId < 0){
            return "" + m.fullArtistList.get(m.fullArtistList.size() - 1);
        }else{
            return "" + m.fullArtistList.get(songId);
        }
        //return "" + m.fullArtistList.get(songId = sp.getInt("songId", 0));
    }

    public CharSequence getAlbum() {
        String ar = (String) getArtist();
        String album = m.albumName.get(m.albumId.indexOf(m.fullalbumIdList.get(songId = sp.getInt("songId", 0))));
        //String album = m.albumName.get(m.artist.indexOf(ar));
        //String album = m.albumName.get(m.title.indexOf(m.fullTitleList.get(songId = sp.getInt("songId", 0))));
        //return "" + m.fullA.get(songId = sp.getInt("songId", 0));
        return album;
    }

    public static class PreviousButtonListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            EgineBackground f = new EgineBackground();
            f.previousSong("notificationBroadcast");
            f.permissionNotify = true;
            if(sp.getBoolean("isForegroud", false)){
                p.update2();
            }
            remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_pause);
            notificationManager.notify(1, notification);
            //f.permissionNotify = true;


            f.updateNotification();
            //p.update2();

//            AppWidgetManager awm = AppWidgetManager.getInstance(context);
//            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
//            ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
//            //rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetplay);
//            rv.setTextViewText(R.id.textView22, "" + f.getTitle());
//            awm.updateAppWidget(thisWidget, rv);
            AppWidgetManager awm = AppWidgetManager.getInstance(context);
            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
            ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
            rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
            rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(sp.getInt("songId", 0)));
            rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
            rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(sp.getInt("songId", 0)));
            rv.setTextViewText(R.id.textView67, "" + m.fullArtistList.get(sp.getInt("songId", 0)));
            if(notification != null){
                Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, notification);
                Picasso.with(context).load(f.getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(rv, R.id.imageView11, 1, notification);
            }
            awm.updateAppWidget(thisWidget, rv);

        }
    }
    public static class NextButtonListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            EgineBackground f = new EgineBackground();
            f.nextSong("notificationBroadcast");
            f.permissionNotify = true;
            if(sp.getBoolean("isForegroud", false)){
                p.update2();
            }
            //f.setNotificationPlayerIcon();
            remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_pause);
            notificationManager.notify(1, notification);

//            AppWidgetManager awm = AppWidgetManager.getInstance(context);
//            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
//            ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
//            //rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetplay);
//            rv.setTextViewText(R.id.textView22, "" + f.getTitle());
//            awm.updateAppWidget(thisWidget, rv);
            AppWidgetManager awm = AppWidgetManager.getInstance(context);
            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
            ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
            rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
            rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(sp.getInt("songId", 0)));
            rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
            rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(sp.getInt("songId", 0)));
            rv.setTextViewText(R.id.textView67, "" + m.fullArtistList.get(sp.getInt("songId", 0)));
            if(notification != null){
                Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, notification);
                Picasso.with(context).load(f.getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(rv, R.id.imageView11, 1, notification);
            }
            awm.updateAppWidget(thisWidget, rv);
            f.updateNotification();
        }
    }
    public static class PlayButtonListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
//            EgineBackground f = new EgineBackground();
//            f.nextSong();
            if(mp.isPlaying()){
                mp.pause();
                editor.putInt("loadLastPosition", mp.getCurrentPosition());
                editor.putBoolean("loadLastPositionChk", true);
                editor.commit();
                if(sp.getBoolean("isForegroud", false)){
                    p.play.setBackgroundResource(sp.getInt("play", R.drawable.play));
                }
                //p.update2();
                remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_play);
                AppWidgetManager awm = AppWidgetManager.getInstance(context);
                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetplay);
                awm.updateAppWidget(thisWidget, rv);
            }else{
                mp.start();
                if(sp.getBoolean("isForegroud", false)){
                    p.play.setBackgroundResource(sp.getInt("play", R.drawable.pause));
                }
                remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_pause);
                AppWidgetManager awm = AppWidgetManager.getInstance(context);
                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                awm.updateAppWidget(thisWidget, rv);
            }
            notificationManager.notify(1, notification);

        }
    }


    public static class CancelButtonListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            EgineBackground e = new EgineBackground();
            Intent i = new Intent(context, EgineBackground.class);

            AppWidgetManager awm = AppWidgetManager.getInstance(context);
            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
            ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
            rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetplay);
            awm.updateAppWidget(thisWidget, rv);
            //Intent myIntent = new Intent(context, Player.class);
            mp.stop();
            mp = null;
            editor.putBoolean("loadLastPositionChk", true);
            editor.commit();
            context.stopService(i);

        }
    }

    public static class CallDetector extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            sp = context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
            editor = sp.edit();
            //if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                //Log.e("In on receive", "In Method:  ACTION_SCREEN_OFF");
                //Toast.makeText(context, "down", Toast.LENGTH_LONG).show();

            //} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                //Log.e("In on receive", "In Method:  ACTION_SCREEN_ON");
                //Toast.makeText(context, "up", Toast.LENGTH_LONG).show();
            //}
//            if(sp.getBoolean("shouldStopOnIncomingCall", true)){
//                //boolean b = false;
//                if(intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)){
//                    if(mp != null){
//                        if(mp.isPlaying()){
//                            //Toast.makeText(context, "detected", Toast.LENGTH_SHORT).show();
//                            mp.pause();
//                            editor.putBoolean("stop", true);
//                            editor.commit();
//                        }
//                    }
//
//                }else if(intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_IDLE)
//                        || intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
//                    //Toast.makeText(context, "detected2", Toast.LENGTH_SHORT).show();
//                    if(mp != null){
//                        if(!mp.isPlaying()){
//                            if(sp.getBoolean("stop", false)){
//                                mp.start();
//                                editor.putBoolean("stop", false);
//                                editor.commit();
//                            }
//                        }
//                    }
//                }
//            }else{
//
//            }
        }
    }
}

