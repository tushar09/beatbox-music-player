package beat.box.music.player;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;


public class EqualizerLocal extends ActionBarActivity implements SeekBar.OnSeekBarChangeListener {
    public static EgineBackground eg = new EgineBackground();
    private static ToggleButton tb;
    private static Spinner preset, presetaReverb;
    private static TextView presetText, reverbText, effects3d, bassBoost;
    private static SeekBar band0, band1, band2, band3, band4, band5, band6, band7, bb, vt;
    private static TextView tv0, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15;
    private static TextView db0, db1, db2, db3, db4, db5, db6, db7;
    public static SeekBar seekbarsArray[] = new SeekBar[8];
    public static TextView textViewFirstHalf[] = new TextView[8];
    public static TextView textViewSecondHalf[] = new TextView[8];
    public static TextView db[] = new TextView[8];
    public short bandRange[];
    public int min_level, max_level;
    private static Switch switch1;
    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    public boolean presetSpinnerFirstTimeChk = false;
    public boolean presetReverbSpinnerFirstTimeChk = false;
    private ArrayList presetName = new ArrayList();
    private ArrayList presetReverb = new ArrayList();
    Tracker t;
    AdView av;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equalizer);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("Ecualizer");
        t.send(new HitBuilders.AppViewBuilder().build());

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();

        av = (AdView) findViewById(R.id.adView2);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        presetText = (TextView) findViewById(R.id.textView37);
        reverbText = (TextView) findViewById(R.id.textView36);
        effects3d = (TextView) findViewById(R.id.textView34);
        bassBoost = (TextView) findViewById(R.id.textView35);
        presetText.setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
        reverbText.setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
        bassBoost.setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
        effects3d.setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.equalizer, null);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setIcon(R.drawable.ic_launcher);
        actionBar.setCustomView(v);
        switch1 = (Switch) findViewById(R.id.switch1);

//        tb.setChecked(sp.getBoolean("toggle", true));
//        tb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    eg.eq.setEnabled(true);
//                    eg.bb.setEnabled(true);
//                    eg.vt.setEnabled(true);
//                    editor.putBoolean("toggle", true);
//                    editor.commit();
//
//                }else{
//                    eg.eq.setEnabled(false);
//                    eg.bb.setEnabled(false);
//                    eg.vt.setEnabled(false);
//                    editor.putBoolean("toggle", false);
//                    editor.commit();
//                }
//            }
//        });


        bb = (SeekBar) findViewById(R.id.bassBoost);
        presetName = new ArrayList();
        if(eg.eq != null){
            int totalPreset = eg.eq.getNumberOfPresets();
            for(int t = 0; t < totalPreset; t++){
                presetName.add(eg.eq.getPresetName((short) t));
            }
            presetName.add("User");
        }

        presetReverb.add("None");
        presetReverb.add("Small Room");
        presetReverb.add("Medium Room");;
        presetReverb.add("Large Room");
        presetReverb.add("Medium Hall");
        presetReverb.add("Large Hall");
        presetReverb.add("Plate");


        preset = (Spinner) findViewById(R.id.preSet);
        presetaReverb = (Spinner) findViewById(R.id.presetReverb);

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getBaseContext(),  android.R.layout.simple_spinner_item, presetReverb);
        dataAdapter2.setDropDownViewResource(R.layout.customspinnermenu);
        presetaReverb.setAdapter(dataAdapter2);
        presetaReverb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                presetReverbSpinnerFirstTimeChk = true;
                return false;
            }
        });
        presetaReverb.setSelection(sp.getInt("spinnerPosition2", 0));
        presetaReverb.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(presetReverbSpinnerFirstTimeChk){
                    eg.pr.setPreset((short) position);
                    //savePreset();
                    editor.putInt("spinnerPosition2", position);
                    editor.commit();
                    //updateSliders();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(),  android.R.layout.simple_spinner_item, presetName);
        dataAdapter.setDropDownViewResource(R.layout.customspinnermenu);
        preset.setAdapter(dataAdapter);
        preset.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                presetSpinnerFirstTimeChk = true;
                return false;
            }
        });
        preset.setSelection(sp.getInt("spinnerPosition", 0));
        preset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(presetSpinnerFirstTimeChk){

                    if(presetName.get(position).equals("User")){
                        loadUserPreset();
                    }else{
                        eg.eq.usePreset((short) position);
                        savePreset();
                    }

                    editor.putInt("spinnerPosition", position);
                    editor.commit();
                    updateSliders();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        vt = (SeekBar) findViewById(R.id.effects3d);

        bb.setMax(1000);
        vt.setMax(1000);
        if(sp.getBoolean("ecuSeekBar", false)){
            bb.setProgressDrawable(getResources().getDrawable(sp.getInt("ecuskPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light)));
            bb.setThumb(getResources().getDrawable(sp.getInt("ecuskThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light)));
            vt.setProgressDrawable(getResources().getDrawable(sp.getInt("ecuskPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light)));
            vt.setThumb(getResources().getDrawable(sp.getInt("ecuskThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light)));
        }else{
            bb.setProgressDrawable(getResources().getDrawable(sp.getInt("ecuskPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light)));
            bb.setThumb(getResources().getDrawable(sp.getInt("ecuskThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light)));
            vt.setProgressDrawable(getResources().getDrawable(sp.getInt("ecuskPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light)));
            vt.setThumb(getResources().getDrawable(sp.getInt("ecuskThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light)));
        }

        bb.setProgress(sp.getInt("bb", 0));
        vt.setProgress(sp.getInt("vt", 0));

        bb.setOnSeekBarChangeListener(this);
        vt.setOnSeekBarChangeListener(this);

        switch1.setChecked(sp.getBoolean("toggle", true));
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    eg.eq.setEnabled(true);
                    eg.bb.setEnabled(true);
                    eg.vt.setEnabled(true);
                    editor.putBoolean("toggle", true);
                    editor.commit();

                }else{
                    eg.eq.setEnabled(false);
                    eg.bb.setEnabled(false);
                    eg.vt.setEnabled(false);
                    editor.putBoolean("toggle", false);
                    editor.commit();
                }
            }
        });
//        tb = (ToggleButton) findViewById(R.id.toggleButton);
//        tb.setChecked(sp.getBoolean("toggle", true));
//        tb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    eg.eq.setEnabled(true);
//                    eg.bb.setEnabled(true);
//                    eg.vt.setEnabled(true);
//                    editor.putBoolean("toggle", true);
//                    editor.commit();
//
//                }else{
//                    eg.eq.setEnabled(false);
//                    eg.bb.setEnabled(false);
//                    eg.vt.setEnabled(false);
//                    editor.putBoolean("toggle", false);
//                    editor.commit();
//                }
//            }
//        });

        db0 = (TextView) findViewById(R.id.textView46);
        db1 = (TextView) findViewById(R.id.textView47);
        db2 = (TextView) findViewById(R.id.textView48);
        db3 = (TextView) findViewById(R.id.textView49);
        db4 = (TextView) findViewById(R.id.textView50);
        db5 = (TextView) findViewById(R.id.textView51);
        db6 = (TextView) findViewById(R.id.textView52);
        db7 = (TextView) findViewById(R.id.textView53);

        tv0 = (TextView) findViewById(R.id.textView44);
        tv1 = (TextView) findViewById(R.id.textView3);
        tv2 = (TextView) findViewById(R.id.textView43);
        tv3 = (TextView) findViewById(R.id.textView26);
        tv4 = (TextView) findViewById(R.id.textView42);
        tv5 = (TextView) findViewById(R.id.textView27);
        tv6 = (TextView) findViewById(R.id.textView28);
        tv7 = (TextView) findViewById(R.id.textView29);
        tv8 = (TextView) findViewById(R.id.textView39);
        tv9 = (TextView) findViewById(R.id.textView5);
        tv10 = (TextView) findViewById(R.id.textView40);
        tv11 = (TextView) findViewById(R.id.textView30);
        tv12 = (TextView) findViewById(R.id.textView41);
        tv13 = (TextView) findViewById(R.id.textView31);
        tv14 = (TextView) findViewById(R.id.textView32);
        tv14 = (TextView) findViewById(R.id.textView33);

        db[0] = db0;
        db[1] = db1;
        db[2] = db2;
        db[3] = db3;
        db[4] = db4;
        db[5] = db5;
        db[6] = db6;
        db[7] = db7;

        textViewFirstHalf[0] = tv0;
        textViewFirstHalf[1] = tv1;
        textViewFirstHalf[2] = tv2;
        textViewFirstHalf[3] = tv3;
        textViewFirstHalf[4] = tv4;
        textViewFirstHalf[5] = tv5;
        textViewFirstHalf[6] = tv6;
        textViewFirstHalf[7] = tv7;

        textViewSecondHalf[0] = tv8;
        textViewSecondHalf[1] = tv9;
        textViewSecondHalf[2] = tv10;
        textViewSecondHalf[3] = tv11;
        textViewSecondHalf[4] = tv12;
        textViewSecondHalf[5] = tv13;
        textViewSecondHalf[6] = tv14;
        textViewSecondHalf[7] = tv15;

        band0 = (SeekBar) findViewById(R.id.seekBar3);
        band1 = (SeekBar) findViewById(R.id.seekBar4);
        band2 = (SeekBar) findViewById(R.id.seekBar5);
        band3 = (SeekBar) findViewById(R.id.seekBar6);
        band4 = (SeekBar) findViewById(R.id.seekBar7);
        band5 = (SeekBar) findViewById(R.id.seekBar8);
        band6 = (SeekBar) findViewById(R.id.seekBar9);
        band7 = (SeekBar) findViewById(R.id.seekBar10);

        seekbarsArray[0] = band0;
        seekbarsArray[1] = band1;
        seekbarsArray[2] = band2;
        seekbarsArray[3] = band3;
        seekbarsArray[4] = band4;
        seekbarsArray[5] = band5;
        seekbarsArray[6] = band6;
        seekbarsArray[7] = band7;
        //Equalizer e = new Equalizer(this, 0);
        for(int t = 0; t < eg.eq.getNumberOfBands(); t++){
            seekbarsArray[t].setVisibility(View.VISIBLE);
            //Rect bound = seekbarsArray[t].getProgressDrawable().getBounds();
            if(sp.getBoolean("inverted", false)){
                seekbarsArray[t].setProgressDrawable(getResources().getDrawable(sp.getInt("ecuskPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light)));
                seekbarsArray[t].setThumb(getResources().getDrawable(sp.getInt("ecuskThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light)));
                textViewFirstHalf[t].setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
                textViewSecondHalf[t].setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
                db[t].setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
                db[t].setText(formatBandLabel(eg.eq.getBandFreqRange((short)t)));
                //vt.setProgressDrawable(getResources().getDrawable(sp.getInt("skPrgressDrable", R.drawable.red_scrubber_progress)));
                //vt.setThumb(getResources().getDrawable(sp.getInt("skThumb", R.drawable.red_scrubber_progress)));
            }else{
                seekbarsArray[t].setProgressDrawable(getResources().getDrawable(sp.getInt("ecuskPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light)));
                seekbarsArray[t].setThumb(getResources().getDrawable(sp.getInt("ecuskThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light)));
                textViewFirstHalf[t].setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
                textViewSecondHalf[t].setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
                db[t].setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
                db[t].setText(formatBandLabel(eg.eq.getBandFreqRange((short)t)));
                //vt.setProgressDrawable(getResources().getDrawable(R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light));
                //vt.setThumb(getResources().getDrawable(R.drawable.updatedseekbars_scrubber_control_selector_holo_light));
            }
            //seekbarsArray[t].setProgressDrawable(getResources().getDrawable(sp.getInt("skPrgressDrable", R.drawable.red_scrubber_progress)));
            //seekbarsArray[t].setThumb(getResources().getDrawable(sp.getInt("skThumb", R.drawable.red_scrubber_progress)));
            //seekbarsArray[t].getProgressDrawable().setBounds(bound);
            textViewFirstHalf[t].setVisibility(View.VISIBLE);
            textViewSecondHalf[t].setVisibility(View.VISIBLE);
            db[t].setVisibility(View.VISIBLE);
            seekbarsArray[t].setOnSeekBarChangeListener(this);
        }

        if(eg.eq != null){
            bandRange = eg.eq.getBandLevelRange();
        }

        min_level = bandRange[0];
        max_level = bandRange[1];
        updateSliders();
    }

    public String formatBandLabel(int[] band) {
        return milliHzToString(band[0]);
    }


    public String milliHzToString(int milliHz) {
        if (milliHz < 1000) return "";
        if (milliHz < 1000000)
            return "" + (milliHz / 1000) + "Hz";
        else
            return "" + (milliHz / 1000000) + "kHz";
    }


    public void updateSliders() {
        for (int i = 0; i < eg.eq.getNumberOfBands(); i++) {
            int level;
            if (eg.eq != null)
                level = eg.eq.getBandLevel((short) i);
            else
                level = 0;
            int pos = 100 * level / (max_level - min_level) + 50;
            seekbarsArray[i].setProgress(pos);
        }
    }




    @Override
    public void onProgressChanged(SeekBar seekBar, int level, boolean fromUser) {
        if (seekBar == bb) {
            if(switch1.isChecked()){
                eg.bb.setStrength((short)level);
            }

        }else if(seekBar == vt){
            if(switch1.isChecked()){
                eg.vt.setStrength((short)level);
            }
        } else if (eg.eq != null) {
            int new_level = min_level + (max_level - min_level) * level / 100;

            for (int i = 0; i < eg.eq.getNumberOfBands(); i++) {
                if (seekbarsArray[i] == seekBar) {
                    if(switch1.isChecked()){
                        eg.eq.setBandLevel((short) i, (short) new_level);
                        //preset.setSelection(presetName.size() - 1);
                    }

                    break;
                }
            }
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        savePreset();
        saveUserPreset();
        preset.setSelection(presetName.indexOf("User"));
    }

    private void savePreset() {
        int number = eg.eq.getNumberOfBands();
        for(int t = 0; t < number; t++){
            String s = "band" + t;
            editor.putInt(s, eg.eq.getBandLevel((short)t));
        }
        editor.putInt("bb", eg.bb.getRoundedStrength());
        editor.putInt("vt", eg.vt.getRoundedStrength());
        //editor.putInt("spinnerPosition2", eg.vt.getRoundedStrength());
        editor.putInt("spinnerPosition", presetName.size() - 1);
        editor.commit();
    }
    private void saveUserPreset() {
        int number = eg.eq.getNumberOfBands();
        for(int t = 0; t < number; t++){
            String s = "userBand" + t;
            editor.putInt(s, eg.eq.getBandLevel((short)t));
        }
        editor.putInt("userBb", eg.bb.getRoundedStrength());
        editor.putInt("userVt", eg.vt.getRoundedStrength());
        editor.commit();
    }
    private void loadUserPreset(){
        if(eg.eq != null){
            for(int t = 0; t < eg.eq.getNumberOfBands(); t++){
                String s = "userBand" + t;
                eg.eq.setBandLevel((short)t, (short)sp.getInt(s, 0));
            }
            eg.bb.setStrength((short) sp.getInt("userBb", 0));
            eg.vt.setStrength((short) sp.getInt("userVt", 0));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(EqualizerLocal.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
        GoogleAnalytics.getInstance(EqualizerLocal.this).reportActivityStart(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
