package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Favourite extends ActionBarActivity {

    public static SharedPreferences sp;
    public static ListView lv;
    Tracker t;
    AdView av;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("Favourite");
        t.send(new HitBuilders.AppViewBuilder().build());

        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        lv = (ListView) findViewById(R.id.listView8);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        actionBar.setDisplayHomeAsUpEnabled(true);

        File mydir = this.getDir("Favourites", Context.MODE_PRIVATE);
        if (!mydir.exists()) {
            mydir.mkdir();
        }
        File fs = new File(mydir, "Favourite");
        ArrayList path = new ArrayList();
        ArrayList duration = new ArrayList();
        ArrayList title = new ArrayList();
        ArrayList artist = new ArrayList();
        ArrayList<Long> albumid = new ArrayList<Long>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fs));
            StringBuilder text = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                String parts[] = line.split("  _ _ _ _  ");
                path.add(parts[0]);
                duration.add(parts[1]);
                title.add(parts[2]);
                artist.add(parts[3]);
                albumid.add(Long.parseLong(parts[4]));

//                for (int t = 0; t < 1; t++) {
//                    Log.e("testing", "" + path.get(t));
//                    Log.e("testing", "" + duration.get(t));
//                    Log.e("testing", "" + title.get(t));
//                    Log.e("testing", "" + artist.get(t));
//                    Log.e("testing", "" + albumid.get(t));
//                }
                //Log.e("testing", "" + line);
            }
            //Log.e("testing", "" + text);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lv.setAdapter(new ListViewFavourite(this, path, duration, title, artist, albumid));

    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(Favourite.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
        GoogleAnalytics.getInstance(Favourite.this).reportActivityStart(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
