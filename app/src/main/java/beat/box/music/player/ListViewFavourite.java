package beat.box.music.player;

import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Tushar on 5/29/2015.
 */
public class ListViewFavourite extends BaseAdapter {

    private Favourite m = new Favourite();
    private static MainActivity f = new MainActivity();
    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    private static LayoutInflater inflater = null;
    public static Dialog dg;
    static ListView lv = null;
    private static EgineBackground eg = new EgineBackground();


    ArrayList path = new ArrayList();
    ArrayList duration = new ArrayList();
    ArrayList title = new ArrayList();
    ArrayList artist = new ArrayList();
    ArrayList<Long> albumid = new ArrayList<Long>();
    Context context;

    public ListViewFavourite(Context context, ArrayList path, ArrayList duration, ArrayList title, ArrayList artist, ArrayList albumid) {
        this.context = context;
        dg = new Dialog(context);
        sp = context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
        this.path = path;
        this.duration = duration;
        this.title = title;
        this.artist = artist;
        this.albumid = albumid;
    }

    @Override
    public int getCount() {
        return path.size();
    }

    @Override
    public Object getItem(int position) {
        return title.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;


        //View rowView = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customlistviewsongssinglerow, null);
            holder = new Holder();
            holder.songTitle = (TextView) convertView.findViewById(R.id.songTitle);
            holder.songTitleArtist = (TextView) convertView.findViewById(R.id.songTitleArtist);
            holder.songDuration = (TextView) convertView.findViewById(R.id.songDuration);
            holder.menu = (ImageButton) convertView.findViewById(R.id.menu);
            convertView.setTag(holder);
        } else {
            //Log.i("TAG","inside");
            holder = (Holder) convertView.getTag();
            //rowView.setTag(holder);
        }
        holder.songTitle.setText("" + title.get(position));
        holder.songTitleArtist.setText("" + artist.get(position));
        final long duratio = Long.parseLong("" + this.duration.get(position));
        int seconds = (int) ((duratio / 1000) % 60);
        long minutes = ((duratio - seconds) / 1000) / 60;
        if (seconds < 10) {
            holder.songDuration.setText("" + minutes + ":0" + seconds);
        } else {

            holder.songDuration.setText("" + minutes + ":" + seconds);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                f.fullPathList = new ArrayList(path);
                f.fullSongDurationList = new ArrayList(duration);
                f.fullTitleList = new ArrayList(title);
                f.fullArtistList = new ArrayList(artist);
                f.fullalbumIdList = new ArrayList<Long>(albumid);
                f.saveSongForWidget();

                editor.putString("uri", "" + f.songsPath.get(position));
                editor.putString("from folder to player", "no");
                editor.putInt("songId", position);
                editor.commit();

                Intent palyer = new Intent(context, Player.class);
                palyer.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                palyer.putExtra("canStart", true);
                context.startActivity(palyer);
                eg.updateNotification();
                eg.setNotificationPlayerIcon();

//                AppWidgetManager awm = AppWidgetManager.getInstance(context);
//                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
//                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
//                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
//                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
//                awm.updateAppWidget(thisWidget, rv);
                AppWidgetManager awm = AppWidgetManager.getInstance(context);
                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView67, "" + f.fullArtistList.get(position));
                if(eg.notification != null){
                    Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                    Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eg.notification);
                    Picasso.with(context).load(getBitMapUri(f.fullalbumIdList.get(position))).into(rv, R.id.imageView11, 1, eg.notification);
                }

                awm.updateAppWidget(thisWidget, rv);
            }


        });

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu pm = new PopupMenu(context, v);
                MenuInflater mi = pm.getMenuInflater();
                mi.inflate(R.menu.favourite, pm.getMenu());
                pm.show();
                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getItemId() == R.id.addtoplaylist) {

                            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View popupView = inflator.inflate(R.layout.activity_play_list, null);

                            dg.setTitle("" + f.fullTitleList.get(position));
                            dg.setContentView(popupView);

                            dg.show();
                            lv = (ListView) dg.findViewById(R.id.listView4);
                            ArrayList ary = new ArrayList();
                            File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                            if (!mydir.exists()) {
                                mydir.mkdir();
                            }
                            File directory = new File(mydir.toString());
                            File[] listPlaylist = directory.listFiles();
                            for (File file : listPlaylist) {
                                if (file.isFile()) {
                                    ary.add(file.getName());
                                } else {

                                }
                            }
                            //showAssets();

                            lv.setAdapter(new DialogPplaylistListview(context, ary, position));
                            Button addNew = (Button) dg.findViewById(R.id.button2);
                            addNew.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Dialog newPlaylsit = new Dialog(context);
                                    newPlaylsit.setContentView(R.layout.addnewplaylist);
                                    newPlaylsit.setTitle("Add New Playlist");
                                    dg.dismiss();


                                    newPlaylsit.show();
                                    ImageButton add = (ImageButton) newPlaylsit.findViewById(R.id.imageButton31);
                                    ImageButton cancel = (ImageButton) newPlaylsit.findViewById(R.id.imageButton30);
                                    final EditText et = (EditText) newPlaylsit.findViewById(R.id.editText2);
                                    add.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (et.getText().toString().equals("") || et.getText().toString().equals(null)) {
                                                Toast.makeText(context, "Empty name can not be added", Toast.LENGTH_LONG).show();
                                            } else {

                                                String listName = et.getText().toString();
                                                File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                                                if (!mydir.exists()) {
                                                    mydir.mkdir();
                                                }
                                                File f = new File(mydir, listName);

                                                try {
                                                    FileOutputStream fos = new FileOutputStream(f, true);
                                                    String s = getMusicInformation(position) + "\n";
                                                    fos.write(s.getBytes());
                                                    fos.close();
                                                } catch (FileNotFoundException e) {
                                                    e.printStackTrace();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                newPlaylsit.dismiss();

                                            }
                                        }
                                    });
                                    cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            newPlaylsit.dismiss();
                                            dg.show();
                                        }
                                    });
                                }
                            });

                        }else if(item.getItemId() == R.id.remove) {
                            removeLine(position);
                            path.remove(position);
                            duration.remove(position);
                            title.remove(position);
                            artist.remove(position);
                            albumid.remove(position);
                            m.lv.setAdapter(new ListViewFavourite(context, path, duration, title, artist, albumid));
                        }else if (item.getItemId() == R.id.share) {
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            ShareMusicFile smf = new ShareMusicFile();
                            String tit = "" + title.get(position);
                            sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(smf.getMusicFilePath(tit))));
                            sendIntent.setType("*/*");
                            context.startActivity(Intent.createChooser(sendIntent, "Share Via"));
                            return true;
                        }
                        return false;
                    }
                });
            }
        });

        return convertView;
    }

    private Uri getBitMapUri(Long album_id) {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
        return uri;
    }

    public static class Holder {
        TextView songTitle, songTitleArtist, songDuration;
        ImageButton menu;
        int position;

    }

    private void removeLine(int position) {
        File mydir = context.getDir("Favourites", Context.MODE_PRIVATE);
        if (!mydir.exists()) {
            mydir.mkdir();
        }
        File f = new File(mydir, "Favourite");
        File temp = new File(mydir, "temp");
        try {
            //BufferedReader reader = new BufferedReader(new FileReader(f));
            BufferedWriter write = new BufferedWriter(new FileWriter(temp));
            for (int t = 0; t < path.size(); t++) {
                if (t != position) {
                    String s = getMusicInformation(t) + "\n";
                    write.write(s);
                }
            }
            write.close();
            temp.renameTo(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getMusicInformation(int position) {
        return
                path.get(position) + "  _ _ _ _  "
                        + duration.get(position) + "  _ _ _ _  "
                        + title.get(position) + "  _ _ _ _  "
                        + artist.get(position) + "  _ _ _ _  "
                        + albumid.get(position) + "  _ _ _ _  ";
    }
}
