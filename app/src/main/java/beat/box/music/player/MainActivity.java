package beat.box.music.player;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.pushbots.push.Pushbots;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class MainActivity extends ActionBarActivity {

    private CustomListViewSongs f;

    private LinearLayout slider;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    public static CheckBox cb;

    public static ActionBar actionBar;
    public PagerTabStrip pts;
    public static boolean handlerChk = false;
    public static ViewPagerParallax pager = null;
    public static TextView actionbarTitle, totalPlayBackTimer, totalItems;
    public static AutoCompleteTextView listViewEditText;
    public static ImageButton listViewSearch, stat;
    public static Context context;
    public static ListView songslist;
    public static GridView songAlbum;
    public static final int MAX_PAGES = 1;
    public static CustomListViewSongs adapter;
    public static my_adapter ma;

    private int num_pages = 4;

    public static ArrayList fullPathList = new ArrayList();
    public static ArrayList fullTitleList = new ArrayList();
    public static ArrayList<Long> fullalbumIdList = new ArrayList<Long>();
    public static ArrayList fullSongDurationList = new ArrayList();
    public static ArrayList fullArtistList = new ArrayList();

    public static ArrayList<String> title;
    public static ArrayList<Long> albumId = new ArrayList<Long>();
    public static ArrayList<String> albumName = new ArrayList<String>();

    public static ArrayList<String> albumNameSorted = new ArrayList<String>();
    public static ArrayList albumNameTotalItem = new ArrayList();
    public static ArrayList albumNameSortedArtist = new ArrayList();
    public static ArrayList albumNamesotedId = new ArrayList();

    public static ArrayList<String> artistNameSorted = new ArrayList<String>();
    public static ArrayList artistNameTotalItem = new ArrayList();
    public static ArrayList artistNameSortedAlbum = new ArrayList();
    public static ArrayList artistNamesotedId = new ArrayList();

    public static ArrayList songs = new ArrayList();
    public static ArrayList<String> artist = new ArrayList();
    public static ArrayList songsPath = new ArrayList();
    public static ArrayList songsDuration = new ArrayList();
    public static ArrayList year = new ArrayList();
    public static ArrayList composer = new ArrayList();

    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    Tracker t;

    AdView av;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("Library");
        t.send(new HitBuilders.AppViewBuilder().build());

        Pushbots.sharedInstance().init(this);
        Pushbots.sharedInstance().regID();
        Pushbots.sharedInstance().setCustomHandler(SendToUpdate.class);

        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);


//        if(!isNetworkAvailable()){


//        }
//        private boolean isNetworkAvailable() {
//            ConnectivityManager connectivityManager
//                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//        }


        File mydir = this.getDir("Playlists", Context.MODE_PRIVATE);
        if (!mydir.exists()) {
            mydir.mkdir();
        }

        slider = (LinearLayout) findViewById(R.id.slider);
        cb = (CheckBox) findViewById(R.id.checkBox);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        //mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        ArrayList slideMenu = new ArrayList();
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                totalItems.setVisibility(View.VISIBLE);
                actionbarTitle.setText("Library");
                getSupportActionBar().setTitle("Library");
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                totalItems.setVisibility(View.GONE);
                actionbarTitle.setText("Quick Setting");
                getSupportActionBar().setTitle("Quick Setting");
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        //populate(this);
        pts = (PagerTabStrip) findViewById(R.id.tabStripe2);
        pts.setBackgroundColor(Color.parseColor("#" + "000000"));
        context = this;
        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();

        if (sp.getBoolean("shouldStopOnIncomingCall", true)) {
            cb.setChecked(true);
        }

        actionBar = getSupportActionBar();


        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f34335")));
        actionBar.setElevation(0);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.customactionbar, null);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setIcon(R.drawable.ic_launcher);
        actionBar.setCustomView(v);
        listViewSearch = (ImageButton) findViewById(R.id.listViewSearch);
        listViewEditText = (AutoCompleteTextView) findViewById(R.id.editText);
        actionbarTitle = (TextView) findViewById(R.id.text_left);
        totalItems = (TextView) findViewById(R.id.textView70);
        stat = (ImageButton) findViewById(R.id.stat);
        stat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, DataBase.class));
            }
        });

        listViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SearchBox.class);
                i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                context.startActivity(i);
            }
        });

        ma = new my_adapter();

        pager = (ViewPagerParallax) findViewById(R.id.pager);
        pager.set_max_pages(MAX_PAGES);
        pager.setBackgroundAsset(R.drawable.untitled);
        pager.setAdapter(ma);

        pager.getAdapter().notifyDataSetChanged();
        if (pager.getCurrentItem() == 4) {
            Log.e("", "asdfasfdasfasdfasfasdf");
        }

        if (savedInstanceState != null) {
            num_pages = savedInstanceState.getInt("num_pages");
            pager.setCurrentItem(savedInstanceState.getInt("current_page"), false);
        }
    }

    public void saveSongForWidget() {
//        File mydir = context.getDir("songForWidget", Context.MODE_PRIVATE);
//        if (!mydir.exists()) {
//            mydir.mkdir();
//        }
//        File f = new File(mydir, "songForWidget");
//        if(f.exists()){
//            f.delete();
//            f = new File(mydir, "songForWidget");
//        }
//        try {
//            FileOutputStream fos = new FileOutputStream(f, true);
////            PrintWriter writer = new PrintWriter(f);
////            writer.print("");
////            writer.close();
//            for(int t = 0; t < fullTitleList.size(); t++){
//                String s = getMusicInformation(t) + "\n";
//                fos.write(s.getBytes());
//            }
//
//            fos.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private String getMusicInformation(int position) {
        return
                fullPathList.get(position) + "  _ _ _ _  "
                        + fullSongDurationList.get(position) + "  _ _ _ _  "
                        + fullTitleList.get(position) + "  _ _ _ _  "
                        + fullArtistList.get(position) + "  _ _ _ _  "
                        + fullalbumIdList.get(position) + "  _ _ _ _  ";
    }

    public void checked(View v) {
        if (cb.isChecked()) {
            //sp.getBoolean("shouldStopOnIncomingCall", true)
            editor.putBoolean("shouldStopOnIncomingCall", false);
            editor.commit();
        } else {
            //sp.getBoolean("shouldStopOnIncomingCall", true)
            editor.putBoolean("shouldStopOnIncomingCall", true);
            editor.commit();
        }
    }


    public void startTheme(View v) {
        Intent i = new Intent(this, Themes.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);
    }

    public void about(View v) {
        Intent i2 = new Intent(this, About.class);
        i2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i2);
    }

    public void startEcualizer(View v) {
        EgineBackground eb = new EgineBackground();
        eb.setUri(sp.getString("uri", "nai"), sp.getInt("songId", 0));
        Intent i2 = new Intent(this, EgineBackground.class);
        i2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startService(i2);
        //EgineBackground eg = new EgineBackground();
        Intent i = new Intent(this, EqualizerLocal.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);
    }

    public void sleep(View v) {
        Intent i = new Intent(this, sleep.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);
    }

    public void myFolder(View v) {
        Intent i = new Intent(this, MyFolders.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);
        //pager.setCurrentItem(4);
    }

    public void rateMe(View v) {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
            //String url = "http://www.amazon.com/TriumphIT-Music-Player/dp/B00ZD2D1T2/ref=sr_1_16?s=mobile-apps&ie=UTF8&qid=1434885049&sr=1-16&keywords=amazon+mp3&pebp=1434885051870&perid=10M3Z3SR7RE5RBMZ1M1N";
            String url = "https://play.google.com/store/apps/details?id=" + appPackageName;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    }

    public void favourite(View v) {
        Intent i = new Intent(this, Favourite.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);
//        pager.setCurrentItem(4);
    }

    public void playList(View v) {
//        Intent i = new Intent(this, sleep.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//        startActivity(i);
        pager.setCurrentItem(3);
    }

    @Override
    protected void onDestroy() {
        //finish();

        super.onDestroy();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("num_pages", num_pages);
        final ViewPagerParallax pager = (ViewPagerParallax) findViewById(R.id.pager);
        outState.putInt("current_page", pager.getCurrentItem());
    }


    public class my_adapter extends PagerAdapter {

        private String tabtitles[] = new String[]{"Songs", "Album", "Artist", "PlayList"};
        View new_view = null;
        View new_view2 = null;
        View new_view3 = null;

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return num_pages;
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {

            LayoutInflater inflater = getLayoutInflater();
            new_view = inflater.inflate(R.layout.page, null);
            new_view.setTag("pos" + 0);
            new_view2 = inflater.inflate(R.layout.albumgrid, null);

            totalPlayBackTimer = (TextView) findViewById(R.id.totalPlaybackTimer);
            songslist = (ListView) new_view.findViewById(R.id.listView);
            songAlbum = (GridView) new_view2.findViewById(R.id.gridView);
            f = new CustomListViewSongs(context, title, artist, songsDuration, songsPath);
            adapter = new CustomListViewSongs(context, title, artist, songsDuration, songsPath);
            if (position == 0) {
                songslist.setAdapter(adapter);
            } else if (position == 1) {
                songAlbum.setAdapter(new CustomListViewAlbum(context, albumNameSorted, albumNamesotedId, albumNameSortedArtist, albumNameTotalItem));
            } else if (position == 2) {
                songslist.setAdapter(new CustomListViewArtist(context, artistNameSorted, artistNameTotalItem));
            } else if (position == 3) {
                File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
                if (!mydir.exists()) {
                    mydir.mkdir();
                }
                File directory = new File(mydir.toString());
                ArrayList ary = new ArrayList();
                ArrayList ary2 = new ArrayList();
                File[] listPlaylist = directory.listFiles();
                for (File file : listPlaylist) {
                    if (file.isFile()) {
                        ary.add(file.getName());
                        ary2.add("");
                    } else {

                    }
                }
                songslist.setAdapter(new CustomListViewplaylist(context, ary, ary2, false));
//                ArrayList a = new ArrayList();
//                Set<String> e = sp.getStringSet("favouritePathList", null);
//                Iterator<String> i = e.iterator();
//                while(i.hasNext()){
//                    a.add(i.next());
//                }

                //songslist.setAdapter(new CustomListViewArtist(context, a, a));
            } else if (position == 4) {
                File mydir = context.getDir("Favourites", Context.MODE_PRIVATE);
                if (!mydir.exists()) {
                    mydir.mkdir();
                }
                File fs = new File(mydir, "Favourite");
                ArrayList path = new ArrayList();
                ArrayList duration = new ArrayList();
                ArrayList title = new ArrayList();
                ArrayList artist = new ArrayList();
                ArrayList<Long> albumid = new ArrayList<Long>();

            }


            if (position == 1) {
                container.addView(new_view2);
            } else {
                container.addView(new_view);
            }
            Log.e("TAG", "" + position);
            //Log.e("pager", "" + pager.getCurrentItem());
            if (position == 1) {
                return new_view2;
            } else {
                return new_view;
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabtitles[position];
        }


    }


    public void populate(Context context) {

        this.albumName = new ArrayList();
        this.artist = new ArrayList();
        this.albumId = new ArrayList();
        this.albumName = new ArrayList();
        this.songs = new ArrayList();
        this.artist = new ArrayList();
        this.songsPath = new ArrayList();
        this.songsDuration = new ArrayList();
        this.year = new ArrayList();
        this.composer = new ArrayList();
        this.title = new ArrayList();

        ArrayList<Long> albumId = new ArrayList<Long>();
        ArrayList artist = new ArrayList();
        ArrayList title = new ArrayList();
        ArrayList album = new ArrayList();
        ArrayList songs = new ArrayList();
        ArrayList songsPath = new ArrayList();
        ArrayList songsDuration = new ArrayList();
        ArrayList year = new ArrayList();
        ArrayList composer = new ArrayList();


        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        Cursor cursor;
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.YEAR,
                MediaStore.Audio.Media.COMPOSER
        };

        cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);
        while (cursor.moveToNext()) {
            artist.add(cursor.getString(1));
            songsPath.add(cursor.getString(3));
            songs.add(cursor.getString(2));
            songsDuration.add(cursor.getString(5));
            album.add(cursor.getString(6));
            albumId.add(cursor.getLong(7));
            title.add(cursor.getString(2));
            year.add(cursor.getString(9));
            composer.add(cursor.getString(10));
        }
        cursor.close();
        this.title = new ArrayList(title);
        Collections.sort(this.title, String.CASE_INSENSITIVE_ORDER);
        for (int t = 0; t < this.title.size(); t++) {
            //if(title.contains(this.title.get(t))){
            int index = title.indexOf(this.title.get(t));
            String s = "" + this.title.get(t);
            String s1 = "" + title.get(index);
            this.albumId.add(albumId.get(index));
            this.albumName.add("" + album.get(index));
            this.songs.add(songs.get(index));
            this.artist.add("" + artist.get(index));
            this.songsPath.add(songsPath.get(index));
            this.songsDuration.add(songsDuration.get(index));
            this.year.add(year.get(index));
            this.composer.add(composer.get(index));

            //}
        }
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (String temp : this.albumName) {
            Integer count = map.get(temp);
            map.put(temp, (count == null) ? 1 : count + 1);
        }
        Map<String, Integer> treeMap = new TreeMap<String, Integer>(map);
        for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
            albumNameSorted.add(entry.getKey());
            albumNameTotalItem.add(entry.getValue());
            albumNameSortedArtist.add(artist.get(album.indexOf(entry.getKey())));
            albumNamesotedId.add(albumId.get(album.indexOf(entry.getKey())));
        }

        for (int t = 0; t < this.albumNameSortedArtist.size(); t++) {
            if (!artistNameSorted.contains(albumNameSortedArtist.get(t))) {
                artistNameSorted.add("" + albumNameSortedArtist.get(t));
            }
        }
        Collections.sort(artistNameSorted, String.CASE_INSENSITIVE_ORDER);
        for (int t = 0; t < artistNameSorted.size(); t++) {
            String ans = artistNameSorted.get(t);
            int counter = 0;
            for (int x = 0; x < albumNameSortedArtist.size(); x++) {
                if (ans.equals(albumNameSortedArtist.get(x))) {
                    counter++;
                }
            }
            artistNameTotalItem.add(counter);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i = new Intent(context, Themes.class);
                i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                context.startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(MainActivity.this).reportActivityStop(this);
        AppEventsLogger.deactivateApp(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (!(activeNetworkInfo != null && activeNetworkInfo.isConnected())) {
            av.setVisibility(View.GONE);
        }
        GoogleAnalytics.getInstance(MainActivity.this).reportActivityStart(this);
        populate(context);
        totalItems.setText("Total: " + songs.size());
        editor.putBoolean("isForegroud", false);
        editor.commit();
        slider.setBackgroundColor(Color.parseColor("#" + sp.getString("color", "f34335")));
        fullPathList = songsPath;
        fullTitleList = title;
        fullalbumIdList = albumId;
        fullSongDurationList = songsDuration;
        fullArtistList = artist;
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        pts.setBackgroundColor(Color.parseColor("#" + sp.getString("color", "f34335")));

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd");
        String formattedDate = df.format(c.getTime());
        Log.e("date", formattedDate);
        int date = Integer.parseInt(formattedDate);
        if (date % sp.getInt("reviewCycle", 3) == 0) {
            if(sp.getBoolean("review", true)){
                editor.putBoolean("review", false);

                new AlertDialog.Builder(this)
                        .setTitle("RATE BEAT BOX")
                        .setMessage("Please rate this app. Your review will help us to improve this player.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                    //String url = "http://www.amazon.com/TriumphIT-Music-Player/dp/B00ZD2D1T2/ref=sr_1_16?s=mobile-apps&ie=UTF8&qid=1434885049&sr=1-16&keywords=amazon+mp3&pebp=1434885051870&perid=10M3Z3SR7RE5RBMZ1M1N";
                                    String url = "https://play.google.com/store/apps/details?id=" + appPackageName;
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);
                                }
                                editor.putInt("reviewCycle", 7);// do nothing
                                editor.commit();
                            }
                        })
                        .setNegativeButton("Never", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putInt("reviewCycle", 7);// do nothing
                                editor.commit();
                            }
                        }).setNeutralButton("Later", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putInt("reviewCycle", 3);// do nothing
                                editor.commit();
                            }
                })
                .setIcon(R.drawable.download)
                .show();

                editor.commit();

            }
            //Toast.makeText(this, "got it", Toast.LENGTH_LONG).show();

        }else {
            editor.putBoolean("review", true);
            editor.commit();
        }

        super.onStart();
        //GoogleAnalytics.getInstance(MainActivity.this).reportActivityStart(this);
    }

    @Override
    public void onBackPressed() {
        if (f.dg.isShowing()) {
            f.dg.dismiss();
        }
        super.onBackPressed();
    }
}