package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;


public class MyFolders extends ActionBarActivity {

    public static ListView lv;
    public static Context context;
    public static ArrayList<Long> albumId;
    public static ArrayList artist;
    public static ArrayList title;
    public static ArrayList album;
    public static ArrayList songs;
    public static ArrayList songsPath;
    public static ArrayList songsDuration;
    public static SharedPreferences sp;
    Tracker t;
    AdView av;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_folders);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("My Folder");
        t.send(new HitBuilders.AppViewBuilder().build());

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        StorageHelper sh = new StorageHelper();
        ArrayList al = new ArrayList();

        //Log.e("phone", System.getenv("EXTERNAL_STORAGE"));
        //Log.e("sd", System.getenv("SECONDARY_STORAGE"));

        if(sh.isExternalStorageAvailable()){
            al.add("Phone Memory");
        }
        if(Environment.isExternalStorageRemovable()){
            Boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
            if(isSDPresent){
                al.add("Micro SD Card");
            }
        }else{

        }


        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        actionBar.setDisplayHomeAsUpEnabled(true);


        context = this;
        populate();

        lv = (ListView) findViewById(R.id.listView3);
        lv.setAdapter(new CustomListViewFolders(context, al));
    }

    private void populate() {
        ArrayList<Long> albumId = new ArrayList<Long>();
        ArrayList artist = new ArrayList();
        ArrayList title = new ArrayList();
        ArrayList album = new ArrayList();
        ArrayList songs = new ArrayList();
        ArrayList songsPath = new ArrayList();
        ArrayList songsDuration = new ArrayList();

        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        Cursor cursor;
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.YEAR,
                MediaStore.Audio.Media.COMPOSER
        };
        cursor = this.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);
        while (cursor.moveToNext()) {
            artist.add(cursor.getString(1));
            songsPath.add(cursor.getString(3));
            songs.add(cursor.getString(2));
            songsDuration.add(cursor.getString(5));
            album.add(cursor.getString(6));
            albumId.add(cursor.getLong(7));
            title.add(cursor.getString(2));
        }
        cursor.close();
        this.artist = artist;
        this.songsPath = songsPath;
        this.songs = songs;
        this.songsDuration = songsDuration;
        this.album = album;
        this.albumId = albumId;
        this.title = title;


    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(MyFolders.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
        GoogleAnalytics.getInstance(MyFolders.this).reportActivityStart(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
