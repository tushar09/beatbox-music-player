package beat.box.music.player;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.widget.RemoteViews;

import com.squareup.picasso.Picasso;


/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {

    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    public static EgineBackground eb = new EgineBackground();
    private static MainActivity m = new MainActivity();
    public static RemoteViews views;
    public static int t;
    public static AppWidgetManager appWidgetManager;
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        sp = context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
        // There may be multiple widgets active, so update all of them
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            updateAppWidget(context, appWidgetManager, appWidgetIds[i]);
        }
    }


    @Override
    public void onEnabled(Context context) {
        //Toast.makeText(context, "chckedenter", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        NewAppWidget.appWidgetManager = appWidgetManager;
        t = appWidgetId;
        editor.putInt("appWidgetId", t);
        editor.commit();

        views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);


        //views.setTextViewText(R.id.appwidget_text, widgetText);
        Intent nb = new Intent(context, NextButtonListenerNotification.class);
        PendingIntent nextButtonNotification = PendingIntent.getBroadcast(context, 0, nb, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent pb = new Intent(context, PlayButtonListenerNotification.class);
        PendingIntent playButtonNotification = PendingIntent.getBroadcast(context, 0, pb, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent gta = new Intent(context, GoToApp.class);
        PendingIntent goToAppNotification = PendingIntent.getBroadcast(context, 0, gta, PendingIntent.FLAG_UPDATE_CURRENT);

        views.setOnClickPendingIntent(R.id.imageButton28, nextButtonNotification);
        views.setOnClickPendingIntent(R.id.imageButton29, playButtonNotification);
        views.setOnClickPendingIntent(R.id.imageView6, goToAppNotification);
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }



    public static class NextButtonListenerNotification extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
            appWidgetManager = AppWidgetManager.getInstance(context);
            views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
            sp = context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
            editor = sp.edit();
            if(eb.mp != null){
                eb.mp.pause();
                eb.nextSong("player");

                views.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);

                Uri path = Uri.parse("android.resource://beat.box.music.player/" + R.drawable.download);
                Picasso.with(context).load(path).into(views, R.id.imageView11, 1, eb.notification);
                Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(views, R.id.imageView11, 1, eb.notification);

                views.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(sp.getInt("songId", 0)));
                views.setTextViewText(R.id.textView67, "" + m.fullArtistList.get(sp.getInt("songId", 0)));
                appWidgetManager.updateAppWidget(t, views);
                eb.mp.start();
                eb.remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_pause);
                eb.notificationManager.notify(1, eb.notification);

            }else{
                Intent i2 = new Intent(context, MainActivity.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i2);
                views.setImageViewResource(R.id.imageButton29, R.drawable.widgetplay);
                views.setTextViewText(R.id.textView22, "" + eb.m.fullTitleList.get(eb.localPosition));
                Uri path = Uri.parse("android.resource://beat.box.music.player/" + R.drawable.download);
                Picasso.with(context).load(path).into(views, R.id.imageView11, 1, eb.notification);
                Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(views, R.id.imageView11, 1, eb.notification);
                appWidgetManager.updateAppWidget(t, views);
            }


        }

        private Uri getBitMapUri(Long album_id) {
            final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
            return uri;
        }
    }

    public static class PlayButtonListenerNotification extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent ebs = new Intent(context, EgineBackground.class);
            ebs.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            MainActivity m = new MainActivity();

            views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
            //appWidgetManager = AppWidgetManager.getInstance(context);
            sp = context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
            editor = sp.edit();
            //views.setImageViewResource(R.id.imageButton29, android.R.drawable.ic_media_pause);
            if(eb.mp == null){
                //eb.setUri(sp.getString("uri", "nai"), sp.getInt("songId", 0));
                Intent i2 = new Intent(context, MainActivity.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i2);
                //views.setImageViewResource(R.id.imageButton29, R.drawable.widgetplay);
            }else{
                if(eb.mp.isPlaying()){
                    eb.mp.pause();
                    eb.remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_play);
                    eb.notificationManager.notify(1, eb.notification);
                    views.setImageViewResource(R.id.imageButton29, R.drawable.widgetplay);
                    Uri path = Uri.parse("android.resource://beat.box.music.player/" + R.drawable.download);
                    Picasso.with(context).load(path).into(views, R.id.imageView11, 1, eb.notification);
                    Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(views, R.id.imageView11, 1, eb.notification);
                    eb.notificationManager.notify(1, eb.notification);
                    eb.setNotificationPlayerIcon();
                }else{
                    eb.mp.start();
                    eb.remoteViews.setImageViewResource(R.id.notPlay, android.R.drawable.ic_media_pause);
                    eb.notificationManager.notify(1, eb.notification);
                    //eb.mp.start();
                    views.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                    //eb.setNotificationPlayerIcon();
                    views.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(sp.getInt("songId", 0)));
                    views.setTextViewText(R.id.textView67, "" + m.fullArtistList.get(sp.getInt("songId", 0)));
                    Uri path = Uri.parse("android.resource://beat.box.music.player/" + R.drawable.download);
                    Picasso.with(context).load(path).into(views, R.id.imageView11, 1, eb.notification);
                    Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(views, R.id.imageView11, 1, eb.notification);
                }
                appWidgetManager.updateAppWidget(t, views);
            }

            //Toast.makeText(context, "chcked", Toast.LENGTH_SHORT).show();
        }

        private Uri getBitMapUri(Long album_id) {
            final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
            return uri;
        }


    }
    public static class GoToApp extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(eb.mp == null){
                Intent i2 = new Intent(context, MainActivity.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i2);
            }else{
                if(eb.mp.isPlaying()){
                    Intent i = new Intent(context, Player.class);
                    i.putExtra("canStart", true);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }else{
                    Intent i = new Intent(context, Player.class);
                    i.putExtra("canStart", false);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }

            }
        }
    }
}


