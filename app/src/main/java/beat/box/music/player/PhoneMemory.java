package beat.box.music.player;

import android.app.ListActivity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PhoneMemory extends ListActivity {

    public static MainActivity m = new MainActivity();
    private static EgineBackground eg = new EgineBackground();
    public MyFolders mf = new MyFolders();

    private List<String> item = null;
    private List<String> path = null;
    private List<String> item2 = null;
    private List<String> item3 = null;
    private String root;
    private TextView myPath;
    private Context context;

    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    private static MainActivity f = new MainActivity();
    Tracker t;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phonememory);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("PhoneMemory");
        t.send(new HitBuilders.AppViewBuilder().build());

        context = this;
        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
        //myPath = (TextView)findViewById(R.id.path);

//        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
//        if(isSDPresent){
//            root = System.getenv("SECONDARY_STORAGE");
//        }else{
//            root = System.getenv("EXTERNAL_STORAGE");
//        }
        if(Environment.isExternalStorageRemovable()){
            Boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
            if(isSDPresent){
                root = System.getenv("SECONDARY_STORAGE");
            }
        }else{
            root = System.getenv("EXTERNAL_STORAGE");
        }

        Log.e("phone", System.getenv("SECONDARY_STORAGE"));

        getDir(root);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }



    private void getDir(String dirPath) {
        //myPath.setText("Location: " + dirPath);
        item = new ArrayList<String>();
        item2 = new ArrayList<String>();
        item3 = new ArrayList<String>();
        path = new ArrayList<String>();
        File f = new File(dirPath);
        File[] files = f.listFiles();

        if (!dirPath.equals(root)) {
            item.add(root);
            path.add(root);
            item.add("back");
            path.add(f.getParent());
        }

        ArrayList folder = new ArrayList();
        ArrayList nonMusicFile = new ArrayList();
        ArrayList music = new ArrayList();

        for (int i = 0; i < files.length; i++) {
            File file = files[i];

            if (!file.isHidden() && file.canRead()) {
                //path.add(file.getPath());
                item2.add(file.getPath());
                if (file.isDirectory()) {
                    folder.add(file.getName() + "/");
                    item3.add(file.getName() + "/");
                    //item2.add(file.getPath());
                } else {
                    if(file.getName().endsWith(".mp3") || file.getName().endsWith(".MP3") || file.getName().endsWith(".wav") || file.getName().endsWith(".WAV") || file.getName().endsWith(".amr") || file.getName().endsWith(".AMR")){
                        music.add(file.getName());
                        item3.add(file.getName());
                        //item2.add(file.getPath());
                    }else{
                        nonMusicFile.add(file.getName());
                        item3.add(file.getName());
                        //item2.add(file.getPath());
                    }
                    //item.add(file.getName());
                }
            }
            String[] g = null;

        }
        Collections.sort(folder, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(music, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(nonMusicFile, String.CASE_INSENSITIVE_ORDER);
        for(int t = 0; t < folder.size(); t++){
            item.add("" + folder.get(t));
            path.add("" + item2.get(item3.indexOf(folder.get(t))));
        }
        for(int t = 0; t < music.size(); t++){
            item.add("" + music.get(t));
            path.add("" + item2.get(item3.indexOf(music.get(t))));
        }
        for(int t = 0; t < nonMusicFile.size(); t++){
            item.add("" + nonMusicFile.get(t));
            path.add("" + item2.get(item3.indexOf(nonMusicFile.get(t))));
        }

        CustomListViewPhoneMemory fileList = new CustomListViewPhoneMemory(context, item);
        //ArrayAdapter<String> fileList =
        //new ArrayAdapter<String>(this, R.layout.row, item);
        setListAdapter(fileList);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        File file = new File(path.get(position));

        if (file.isDirectory()) {
            if (file.canRead()) {
                getDir(path.get(position));
            } else {
                Toast.makeText(context, "Sorry, No Access", Toast.LENGTH_LONG).show();
            }
        } else {
//            new AlertDialog.Builder(this)
//                    .setIcon(R.drawable.ic_launcher)
//                    .setTitle("[" + file.getName() + "]")
//                    .setPositiveButton("OK", null).show();
            String s = file.toString();
            if(s.endsWith(".mp3") || s.endsWith(".MP3") || s.endsWith(".wav") || s.endsWith(".WAV") || s.endsWith(".AMR") || s.endsWith(".amr")){

                ArrayList pathlist = new ArrayList();
                ArrayList title = new ArrayList();
                ArrayList albumId = new ArrayList();
                ArrayList duration = new ArrayList();
                ArrayList artist = new ArrayList();


                for(int t = 0; t < path.size(); t++){
                    String g = path.get(t);
                    if(g.endsWith(".mp3") || g.endsWith(".MP3") || g.endsWith(".wav") || g.endsWith(".WAV") || g.endsWith(".amr") || g.endsWith(".AMR")){
                        pathlist.add(path.get(t));
                        title.add(item.get(t));
                        albumId.add(getAlbumId(path.get(position)));
                        duration.add(getDuration(path.get(position)));
                        artist.add(getArtist(path.get(position)));
                    }
                }
                m.fullPathList = new ArrayList(pathlist);
                m.fullTitleList = new ArrayList(title);
                m.fullalbumIdList = new ArrayList(albumId);
                m.fullSongDurationList = new ArrayList(duration);
                m.fullArtistList = new ArrayList(artist);
                m.saveSongForWidget();

                Intent palyer = new Intent(context, Player.class);
                palyer.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                palyer.putExtra("canStart", true);
                editor.putString("uri", "" + m.fullPathList.get(pathlist.indexOf(s)));
                editor.putInt("songId", pathlist.indexOf(s));
                Log.e("latest position checking", "" + position);
                editor.commit();
                context.startActivity(palyer);

//                AppWidgetManager awm = AppWidgetManager.getInstance(context);
//                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
//                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
//                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
//                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
//                awm.updateAppWidget(thisWidget, rv);

                AppWidgetManager awm = AppWidgetManager.getInstance(context);
                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView22, "" + f.fullTitleList.get(position));
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView67, "" + f.fullArtistList.get(position));
                if(eg.notification != null){
                    Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                    Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eg.notification);
                    Picasso.with(context).load(getBitMapUri(f.fullalbumIdList.get(position))).into(rv, R.id.imageView11, 1, eg.notification);
                }

                awm.updateAppWidget(thisWidget, rv);

            }

        }
    }

    private Uri getBitMapUri(Long album_id) {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
        return uri;
    }

    private Object getArtist(String s) {
        if(mf.songsPath.contains(s)){
            return mf.artist.get(mf.songsPath.indexOf(s));
        }else {
            return null;
        }
    }

    private Object getDuration(String s) {
        if(mf.songsPath.contains(s)){
            return mf.songsDuration.get(mf.songsPath.indexOf(s));
        }else {
            return null;
        }
    }

    private Object getAlbumId(String s) {
        if(mf.songsPath.contains(s)){
            return mf.albumId.get(mf.songsPath.indexOf(s));
        }else {
            return null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(PhoneMemory.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(PhoneMemory.this).reportActivityStart(this);
    }
}
