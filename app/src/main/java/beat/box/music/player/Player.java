package beat.box.music.player;

import android.annotation.TargetApi;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class Player extends ActionBarActivity implements GestureDetector.OnGestureListener {

    public static RelativeLayout controlBase;
    public static ActionBar actionBar;
    private ArrayList fullSongList;
    private static MainActivity m = new MainActivity();
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    private static Context context;
    public static ImageButton listViewSearch, stat;

    static boolean nextb = false, previousb = false;
    static EgineBackground eb = new EgineBackground();
    static ImageButton play, next, previous, repeat, suffel;
    static TextView timer, duration, songInfo, actionBarTitle;
    static SeekBar seekbar;
    static ViewSwitcher vs;
    static ImageView base, actionBarAlbumArt;
    private static Handler handler;
    private static Runnable updateTask;
    private static boolean handlerCheck = false;
    private boolean update2Check = true;
    boolean suffelChk;

//    public int [] repeatOneDrawable = new int[1];
//    public int [] repeatAllDrawable = new int[1];
//    public int [] suffleActivatedDrawable = new int[1];

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_second);
        Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("Player");
        t.send(new HitBuilders.AppViewBuilder().build());

        controlBase = (RelativeLayout) findViewById(R.id.controlBase);

        Bundle bundle = getIntent().getExtras();

        actionBar = getSupportActionBar();
        //actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f34335")));
        actionBar.setElevation(0);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //if(Build.VERSION.SDK_INT > 10){
        View v = inflator.inflate(R.layout.customactionbarforplayer, null);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setIcon(R.drawable.ic_launcher);
        actionBar.setCustomView(v);

        actionBarTitle = (TextView) findViewById(R.id.text_left);
        stat = (ImageButton) findViewById(R.id.stat);
        listViewSearch = (ImageButton) findViewById(R.id.listViewSearch);
        actionBarAlbumArt = (ImageView) findViewById(R.id.imageView2);
        listViewSearch.setBackgroundResource(R.drawable.notstatt);
        listViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Player.this, DataBase.class));
            }
        });
        stat.setBackgroundResource(R.drawable.customactionbarequicon);

        //}
        stat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startService(ebs);
                Intent i = new Intent(Player.this, EqualizerLocal.class);
                i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(new Intent(Player.this, EqualizerLocal.class));
            }
        });


        context = this;
        timer = (TextView) findViewById(R.id.timer);
        duration = (TextView) findViewById(R.id.duration);
        songInfo = (TextView) findViewById(R.id.songInfo);
        songInfo.setSelected(true);
        songInfo.setMovementMethod(new ScrollingMovementMethod());

        base = (ImageView) findViewById(R.id.base);


        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int wwidth = displaymetrics.widthPixels;
        //Log.e("Sereen Regulation", "" + wwidth + " * " + height );
        base.getLayoutParams().width = wwidth + (wwidth / 2);
        TranslateAnimation animation = new TranslateAnimation(0.0f, -wwidth / 2, 0.0f, 0.0f);


        animation.setDuration(6000);
        animation.setRepeatCount(100000);
        animation.setRepeatMode(2);
        animation.setFillAfter(true);

        base.startAnimation(animation);

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();


        eb.setUri(sp.getString("uri", "nai"), sp.getInt("songId", 0));
        final Intent ebs = new Intent(this, EgineBackground.class);
        ebs.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        //ebs.putExtra("fromPlayer", "fromPlayer");
        if (bundle.getBoolean("canStart")) {
            //editor.putInt("loadLastPosition", eb.mp.getCurrentPosition());
            editor.putBoolean("loadLastPositionChk", false);
            editor.commit();
            startService(ebs);
            eb.setNotificationPlayerIcon();
        }
        //startService(ebs);

        repeat = (ImageButton) findViewById(R.id.repeat);
        int repeatChk = sp.getInt("repeat", 0);
        if (repeatChk == 0) {
            repeat.setBackgroundResource(R.drawable.repeat);
        } else if (repeatChk == 1) {
            repeat.setBackgroundResource(R.drawable.repeatactivatedone);
        } else if (repeatChk == 2) {
            repeat.setBackgroundResource(R.drawable.repeatactivatedall);
        }

        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int repeatChk = sp.getInt("repeat", 0);
                if (repeatChk == 0) {
                    editor.putInt("repeat", 1);
                    editor.commit();
                    repeat.setBackgroundResource(sp.getInt("repeatOne", R.drawable.repeatactivatedone));
                    //repeat.setBackgroundResource(R.drawable.repeatactivatedone);
                    Toast.makeText(Player.this, "Will be repeating this song", Toast.LENGTH_SHORT).show();
                } else if (repeatChk == 1) {
                    editor.putInt("repeat", 2);
                    editor.commit();
                    repeat.setBackgroundResource(sp.getInt("repeatAll", R.drawable.repeatactivatedall));
                    //repeat.setBackgroundResource(R.drawable.repeatactivatedall);
                    Toast.makeText(Player.this, "Will be repeating all song", Toast.LENGTH_SHORT).show();
                } else if (repeatChk == 2) {
                    editor.putInt("repeat", 0);
                    editor.commit();
                    //repeat.setBackgroundResource(sp.getInt("repeatAll", R.drawable.repeatactivatedall));
                    repeat.setBackgroundResource(R.drawable.repeat);
                    Toast.makeText(Player.this, "Repeat mode off", Toast.LENGTH_SHORT).show();
                }
            }
        });

        suffel = (ImageButton) findViewById(R.id.suffle);
        suffelChk = sp.getBoolean("suffel", false);
        if (suffelChk) {
            suffel.setBackgroundResource(R.drawable.suffleactivated);
        } else {
            suffel.setBackgroundResource(R.drawable.suffle);
        }
        suffel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean suffelChk = sp.getBoolean("suffel", false);
                if (suffelChk) {

                    suffel.setBackgroundResource(R.drawable.suffle);
                    editor.putBoolean("suffel", false);
                    editor.commit();
                    Toast.makeText(Player.this, "Suffel off", Toast.LENGTH_SHORT).show();
                } else {
                    suffel.setBackgroundResource(sp.getInt("suffelActivated", R.drawable.suffleactivated));
                    //suffel.setBackgroundResource(R.drawable.suffleactivated);
                    editor.putBoolean("suffel", true);
                    editor.commit();
                    Toast.makeText(Player.this, "Suffel on", Toast.LENGTH_SHORT).show();
                }
            }
        });

        play = (ImageButton) findViewById(R.id.play);
        //if(eb.mp.isPlaying()){
        play.setBackgroundResource(R.drawable.pause);
        //}
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eb.mp != null) {
                    if (eb.mp.isPlaying()) {
                        eb.mp.pause();
                        play.setBackgroundResource(sp.getInt("play", R.drawable.play));
                        editor.putInt("loadLastPosition", eb.mp.getCurrentPosition());
                        editor.putBoolean("loadLastPositionChk", true);
                        editor.putInt("songId", eb.localPosition);
                        editor.commit();
                        AppWidgetManager awm = AppWidgetManager.getInstance(context);
                        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                        ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                        rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetplay);
                        rv.setTextViewText(R.id.textView22, "" + eb.m.fullTitleList.get(eb.localPosition));
                        rv.setTextViewText(R.id.textView67, "" + eb.m.fullArtistList.get(sp.getInt("songId", 0)));

                        Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                        Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eb.notification);
                        Picasso.with(context).load(getBitMapUri(eb.m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(rv, R.id.imageView11, 1, eb.notification);

                        awm.updateAppWidget(thisWidget, rv);
                        //eb.setNotificationPlayerIcon();
                        stopService(ebs);
                        releaseAllMediaStuff();
                    } else {
                        editor.putInt("songId", eb.localPosition);
                        editor.commit();
                        //eb.mp.start();
                        play.setBackgroundResource(sp.getInt("pause", R.drawable.pause));


                        //editor.putBoolean("loadLastPositionChk", false);
                        //editor.commit();
                        startService(ebs);
                        eb.setNotificationPlayerIcon2();
                        AppWidgetManager awm = AppWidgetManager.getInstance(context);
                        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                        ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                        rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                        rv.setTextViewText(R.id.textView22, "" + eb.m.fullTitleList.get(eb.localPosition));
                        rv.setTextViewText(R.id.textView67, "" + eb.m.fullArtistList.get(sp.getInt("songId", 0)));

                        Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                        Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eb.notification);
                        Picasso.with(context).load(getBitMapUri(eb.m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(rv, R.id.imageView11, 1, eb.notification);

                        awm.updateAppWidget(thisWidget, rv);
                    }
                } else {
                    startService(ebs);
                }
            }
        });

        next = (ImageButton) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekbar.setProgress(0);
                eb.mp.pause();
                eb.nextSong("player");
                //stopService(ebs);
                editor.putBoolean("loadLastPositionChk", false);
                editor.commit();
                startService(ebs);
                update2();

                AppWidgetManager awm = AppWidgetManager.getInstance(context);
                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView22, "" + eb.m.fullTitleList.get(eb.localPosition));
                rv.setTextViewText(R.id.textView67, "" + eb.m.fullArtistList.get(sp.getInt("songId", 0)));

                Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eb.notification);
                Picasso.with(context).load(getBitMapUri(eb.m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(rv, R.id.imageView11, 1, eb.notification);

                awm.updateAppWidget(thisWidget, rv);
            }
        });

        previous = (ImageButton) findViewById(R.id.previous);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekbar.setProgress(0);
                eb.mp.pause();
                eb.previousSong("player");
                editor.putBoolean("loadLastPositionChk", false);
                editor.commit();
                startService(ebs);
                update2();

                AppWidgetManager awm = AppWidgetManager.getInstance(context);
                RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                rv.setTextViewText(R.id.textView22, "" + eb.m.fullTitleList.get(eb.localPosition));
                rv.setTextViewText(R.id.textView67, "" + eb.m.fullArtistList.get(sp.getInt("songId", 0)));

                Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eb.notification);
                Picasso.with(context).load(getBitMapUri(eb.m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(rv, R.id.imageView11, 1, eb.notification);

                awm.updateAppWidget(thisWidget, rv);
            }
        });

        seekbar = (SeekBar) findViewById(R.id.seekBar);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser) {
                    int seconds = (eb.mp.getCurrentPosition() / 1000) % 60;
                    long minutes = ((eb.mp.getCurrentPosition() - seconds) / 1000) / 60;
                    if (seconds < 10) {
                        timer.setText("" + minutes + ":0" + seconds);
                    } else {
                        timer.setText("" + minutes + ":" + seconds);
                    }
                } else {
                    String s;
                    int seconds = (eb.mp.getCurrentPosition() / 1000) % 60;
                    long minutes = ((eb.mp.getCurrentPosition() - seconds) / 1000) / 60;
                    if (seconds < 10) {
                        s = ("" + minutes + ":0" + seconds);
                    } else {
                        s = ("" + minutes + ":" + seconds);
                    }

                    String s2;
                    int seconds2 = (progress / 1000) % 60;
                    long minutes2 = ((progress - seconds2) / 1000) / 60;
                    if (seconds2 < 10) {
                        s2 = ("" + minutes2 + ":0" + seconds2);
                    } else {
                        s2 = ("" + minutes2 + ":" + seconds2);
                    }

                    timer.setText(s + " / " + s2);

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                eb.mp.seekTo(seekBar.getProgress());
            }
        });


        handler = new Handler();
        updateTask = new Runnable() {
            @Override
            public void run() {
                update();
                handler.postDelayed(this, 1000);
            }
        };
        if (!handlerCheck) {
            handler.postDelayed(updateTask, 0);
            handlerCheck = true;
        }

    }

    private void releaseAllMediaStuff() {
//        handler.removeCallbacks(updateTask);
//        eb.mp.release();
//        eb.eq.release();
//        eb.bb.release();
//        eb.vt.release();
//        eb.pr.release();
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(Player.this).reportActivityStop(this);
    }

    @Override
    protected void onStart() {
        GoogleAnalytics.getInstance(Player.this).reportActivityStart(this);
        songInfo.setSelected(true);
        songInfo.requestFocus();
        editor.putBoolean("update2Check", true);
        editor.putBoolean("isForegroud", true);


        if (sp.getBoolean("inverted", false)) {

            int repeatChk = sp.getInt("repeat", 0);
            if (repeatChk == 0) {
                repeat.setBackgroundResource(R.drawable.repeat);
            } else if (repeatChk == 1) {
                repeat.setBackgroundResource(sp.getInt("repeatOne", R.drawable.repeat));
            } else if (repeatChk == 2) {
                repeat.setBackgroundResource(sp.getInt("repeatAll", R.drawable.repeatactivatedall));
            }

            //songInfo.setBackgroundColor(Color.parseColor("#99" + "000000"));
            songInfo.setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
            //timer.setBackgroundColor(Color.parseColor("#99" + "000000"));
            timer.setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
            duration.setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
            controlBase.setBackgroundColor(Color.parseColor("#99" + "000000"));
            Rect bound = seekbar.getProgressDrawable().getBounds();
            //seekbar.setBackgroundColor(Color.parseColor("#99" + "000000"));
            seekbar.setProgressDrawable(getResources().getDrawable(sp.getInt("skPrgressDrable", R.drawable.red_scrubber_progress)));
            seekbar.setThumb(getResources().getDrawable(sp.getInt("skThumb", R.drawable.red_scrubber_progress)));
            seekbar.getProgressDrawable().setBounds(bound);
            previous.setBackgroundResource(sp.getInt("previous", R.drawable.previous));
            play.setBackgroundResource(sp.getInt("pause", R.drawable.pause));
            next.setBackgroundResource(sp.getInt("next", R.drawable.next));
            suffel.setBackgroundResource(sp.getInt("suffelActivated", R.drawable.suffleactivated));
            if (suffelChk) {
                suffel.setBackgroundResource(sp.getInt("suffelActivated", R.drawable.suffleactivated));
            } else {
                suffel.setBackgroundResource(R.drawable.suffle);
            }
        } else {
            //songInfo.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
            //timer.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
            //seekbar.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
            controlBase.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
        }


        editor.commit();

        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        //songInfo.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
        //timer.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
        //duration.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
        //seekbar.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
        //controlBase.setBackgroundColor(Color.parseColor("#99" + sp.getString("color", "f34335")));
        //Log.e("Paused Player", "Player is in started");
        super.onStart();
    }

    @Override
    protected void onPause() {
        //Log.e("Paused Player", "Player is in backgdround");
        editor.putBoolean("isForegroud", false);
        editor.commit();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
//        Intent i = new Intent(this, MainActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//        startActivity(i);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        base = null;
        play = null;
        //Log.e("Paused Player", "Player is in destroyed");
        super.onDestroy();
    }

    public void update2() {
        songInfo.setText("" + m.fullTitleList.get(sp.getInt("songId", 0)));
        seekbar.setMax(eb.mp.getDuration());
        if (sp.getString("from folder to player", "no").equals("yes")) {
            MyFolders mf = new MyFolders();
            actionBarTitle.setText("" + m.fullArtistList.get(sp.getInt("songId", 0)) + "    " + mf.album.get(mf.artist.indexOf(m.fullArtistList.get(sp.getInt("songId", 0)))));
            actionBarTitle.setSelected(true);
            Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(actionBarAlbumArt);
            Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).placeholder(R.drawable.progress_animation).into(base);
        } else {
            actionBarTitle.setText("" + m.fullArtistList.get(sp.getInt("songId", 0)) + "    " + m.albumName.get(m.artist.indexOf(m.fullArtistList.get(sp.getInt("songId", 0)))));
            actionBarTitle.setSelected(true);
            Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).into(actionBarAlbumArt);
            Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).placeholder(R.drawable.progress_animation).into(base);
        }

        //Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(sp.getInt("songId", 0)))).placeholder(R.drawable.progress_animation).into(base);

        int seconds = (eb.mp.getDuration() / 1000) % 60;
        long minutes = ((eb.mp.getDuration() - seconds) / 1000) / 60;
        if (seconds < 10) {
            duration.setText("" + minutes + ":0" + seconds);
        } else {
            duration.setText("" + minutes + ":" + seconds);
        }
    }

    private void update() {

        if (sp.getBoolean("update2Check", true)) {
            update2();
            editor.putBoolean("update2Check", false);
            editor.commit();
            if(eb.mp != null){
                if(eb.mp.isPlaying()){
                    eb.updateNotification();
                }
            }

        }
        if (eb.mp != null) {
            if (eb.mp.isPlaying()) {
                editor.putLong("totalTime", sp.getLong("totalTime", 0) + 1);
                editor.commit();

                //m.totalPlayBackTimer.setText("" + sp.getLong("totalTime", 0));


                seekbar.setProgress(eb.mp.getCurrentPosition());
            }
        }


    }


    private Uri getBitMapUri(Long album_id) {
        //Log.e("album id", "" + album_id);
        //Bitmap bm = null;
        //try {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
        //ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "r");
        //if (pfd != null) {
        //FileDescriptor fd = pfd.getFileDescriptor();
        //bm = BitmapFactory.decodeFileDescriptor(fd);
        //} else {
        //bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.iconforgrid);
        //}

        //} catch (Exception e) {

        //}
        //return bm;
        return uri;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.addToFavourite) {
            File mydir = context.getDir("Favourites", Context.MODE_PRIVATE);
            if (!mydir.exists()) {
                mydir.mkdir();
            }
            File f = new File(mydir, "Favourite");
            try {
                FileOutputStream fos = new FileOutputStream(f, true);
                String s = getMusicInformation(sp.getInt("songId", 0)) + "\n";
                fos.write(s.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        if (id == R.id.themes) {
            Intent i = new Intent(this, Themes.class);
            i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i);
            return true;
        }
        if (id == R.id.equalizer) {
            Intent i = new Intent(this, EqualizerLocal.class);
            i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i);
            return true;
        }
        if (id == R.id.share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File("" + m.fullPathList.get(sp.getInt("songId", 0)))));
            sendIntent.setType("*/*");
            startActivity(Intent.createChooser(sendIntent, "Share Via"));
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private String getMusicInformation(int position) {
        return
                m.fullPathList.get(position) + "  _ _ _ _  "
                        + m.fullSongDurationList.get(position) + "  _ _ _ _  "
                        + m.fullTitleList.get(position) + "  _ _ _ _  "
                        + m.fullArtistList.get(position) + "  _ _ _ _  "
                        + m.fullalbumIdList.get(position) + "  _ _ _ _  ";
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}