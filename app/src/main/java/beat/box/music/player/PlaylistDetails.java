package beat.box.music.player;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class PlaylistDetails extends ActionBarActivity {

    private static EgineBackground eg = new EgineBackground();
    private static MainActivity m = new MainActivity();
    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    String listName = "";
    private static ListView lv;
    private Context context;
    Tracker t;
    AdView av;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_details);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("PlayList Details");
        t.send(new HitBuilders.AppViewBuilder().build());

        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        context = this;
        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
        Bundle b = getIntent().getExtras();
        listName = b.getString("playlistName");
        getSupportActionBar().setTitle(listName);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lv = (ListView) findViewById(R.id.listView5);
        lv.setAdapter(new loadlistview());
        ArrayList a = null;
        //loadlistview l = new loadlistview(a);
    }

    public class loadlistview extends BaseAdapter {

        ArrayList path = new ArrayList();
        ArrayList duration = new ArrayList();
        ArrayList title = new ArrayList();
        ArrayList artist = new ArrayList();
        ArrayList<Long> albumid = new ArrayList<Long>();

        loadlistview() {
            File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
            if (!mydir.exists()) {
                mydir.mkdir();
            }
            File fs = new File(mydir, listName);
            try {
                BufferedReader br = new BufferedReader(new FileReader(fs));
                StringBuilder text = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    text.append(line);
                    String parts[] = line.split("  _ _ _ _  ");
                    path.add(parts[0]);
                    duration.add(parts[1]);
                    title.add(parts[2]);
                    artist.add(parts[3]);
                    albumid.add(Long.parseLong(parts[4]));

                    for (int t = 0; t < 1; t++) {
                        Log.e("testing", "" + path.get(t));
                        Log.e("testing", "" + duration.get(t));
                        Log.e("testing", "" + title.get(t));
                        Log.e("testing", "" + artist.get(t));
                        Log.e("testing", "" + albumid.get(t));
                    }
                    //Log.e("testing", "" + line);
                }
                //Log.e("testing", "" + text);
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getCount() {

            return path.size();
        }

        @Override
        public Object getItem(int position) {
            return title.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = li.inflate(R.layout.customlistviewplaylistsinglerow, null);
            TextView titles = (TextView) v.findViewById(R.id.songTitle);
            TextView artists = (TextView) v.findViewById(R.id.songTitleArtist);
            TextView durations = (TextView) v.findViewById(R.id.songDuration);
            ImageButton ib = (ImageButton) v.findViewById(R.id.menu);
            titles.setText("" + this.title.get(position));
            artists.setText("" + this.artist.get(position));
            long duratio = Long.parseLong("" + duration.get(position));
            int seconds = (int) ((duratio / 1000) % 60);
            long minutes = ((duratio - seconds) / 1000) / 60;
            if (seconds < 10) {
                durations.setText("" + minutes + ":0" + seconds);
            } else {

                durations.setText("" + minutes + ":" + seconds);
            }
            ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeLine(position);
                    lv.setAdapter(new loadlistview());

                }
            });
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    m.fullPathList = new ArrayList(path);
                    m.fullSongDurationList = new ArrayList(duration);
                    m.fullTitleList = new ArrayList(title);
                    m.fullArtistList = new ArrayList(artist);
                    m.fullalbumIdList = new ArrayList<Long>(albumid);
                    m.saveSongForWidget();
                    Intent palyer = new Intent(context, Player.class);
                    palyer.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    palyer.putExtra("canStart", true);
                    editor.putString("uri", "" + m.fullPathList.get(position));

                    context.startActivity(palyer);
                    editor.putInt("songId", position);
                    editor.commit();

                    eg.updateNotification();
                    eg.setNotificationPlayerIcon();

//                    AppWidgetManager awm = AppWidgetManager.getInstance(context);
//                    RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
//                    ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
//                    rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
//                    rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(position));
//                    awm.updateAppWidget(thisWidget, rv);

                    AppWidgetManager awm = AppWidgetManager.getInstance(context);
                    RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
                    ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
                    rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                    rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(position));
                    rv.setImageViewResource(R.id.imageButton29, R.drawable.widgetpause);
                    rv.setTextViewText(R.id.textView22, "" + m.fullTitleList.get(position));
                    rv.setTextViewText(R.id.textView67, "" + m.fullArtistList.get(position));
                    if(eg.notification != null){
                        Uri path = Uri.parse("android.resource://prime.beatbox/" + R.drawable.download);
                        Picasso.with(context).load(path).into(rv, R.id.imageView11, 1, eg.notification);
                        Picasso.with(context).load(getBitMapUri(m.fullalbumIdList.get(position))).into(rv, R.id.imageView11, 1, eg.notification);
                    }

                    awm.updateAppWidget(thisWidget, rv);

                }
            });
            return v;
        }

        private void removeLine(int position) {
            File mydir = context.getDir("Playlists", Context.MODE_PRIVATE);
            if (!mydir.exists()) {
                mydir.mkdir();
            }
            File f = new File(mydir, listName);
            File temp = new File(mydir, "temp");
            try {
                //BufferedReader reader = new BufferedReader(new FileReader(f));
                BufferedWriter write = new BufferedWriter(new FileWriter(temp));
                for (int t = 0; t < path.size(); t++) {
                    if (t != position) {
                        String s = getMusicInformation(t) + "\n";
                        write.write(s);
                    }
                }
                write.close();
                temp.renameTo(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private String getMusicInformation(int position) {
            return
                    path.get(position) + "  _ _ _ _  "
                            + duration.get(position) + "  _ _ _ _  "
                            + title.get(position) + "  _ _ _ _  "
                            + artist.get(position) + "  _ _ _ _  "
                            + albumid.get(position) + "  _ _ _ _  ";
        }
    }

    private Uri getBitMapUri(Long album_id) {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
        return uri;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(PlaylistDetails.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
        GoogleAnalytics.getInstance(PlaylistDetails.this).reportActivityStart(this);
    }
}
