package beat.box.music.player;

import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class SearchAlbum extends ActionBarActivity {

    ImageView iv;
    AdView av;
    public static ListView lv;
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;

    public static ArrayList<Long> albumId = new ArrayList<Long>();
    public static ArrayList artist = new ArrayList();
    public static ArrayList title = new ArrayList();
    public static ArrayList album = new ArrayList();
    public static ArrayList songs = new ArrayList();
    public static ArrayList songsPath = new ArrayList();
    public static ArrayList songsDuration = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_album);

        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        populate();
        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();

        iv = (ImageView) findViewById(R.id.imageView9);
        lv = (ListView) findViewById(R.id.listView9);
        iv.setImageResource(R.drawable.picture);
        Bundle b = getIntent().getExtras();
        String albumName = b.getString("albumName");

        ArrayList title = new ArrayList();
        ArrayList artist = new ArrayList();
        ArrayList duration = new ArrayList();
        ArrayList path = new ArrayList();
        ArrayList albumIdInner = new ArrayList();

        for(int t = 0; t < album.size(); t++){
            if(album.get(t).equals(albumName)){
                title.add(this.title.get(t));
                artist.add(this.artist.get(t));
                duration.add(this.songsDuration.get(t));
                path.add(this.songsPath.get(t));
                albumIdInner.add(this.albumId.get(t));
            }
        }

        getSupportActionBar().setTitle(albumName);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Long id = albumId.get(album.indexOf(albumName));
        Picasso.with(SearchAlbum.this).load(getBitMapUri(id)).into(iv);

        lv.setAdapter(new CustomListViewSongsAlbum(SearchAlbum.this, title, artist, duration, path, albumIdInner));

    }

    private Uri getBitMapUri(Long album_id) {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
        return uri;
    }

    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    private void populate() {
        ArrayList<Long> albumId = new ArrayList<Long>();
        ArrayList artist = new ArrayList();
        ArrayList title = new ArrayList();
        ArrayList album = new ArrayList();
        ArrayList songs = new ArrayList();
        ArrayList songsPath = new ArrayList();
        ArrayList songsDuration = new ArrayList();

        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        Cursor cursor;
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.YEAR,
                MediaStore.Audio.Media.COMPOSER
        };
        cursor = this.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);
        while (cursor.moveToNext()) {
            artist.add(cursor.getString(1));
            songsPath.add(cursor.getString(3));
            songs.add(cursor.getString(2));
            songsDuration.add(cursor.getString(5));
            album.add(cursor.getString(6));
            albumId.add(cursor.getLong(7));
            title.add(cursor.getString(2));
        }
        cursor.close();
        this.artist = artist;
        this.songsPath = songsPath;
        this.songs = songs;
        this.songsDuration = songsDuration;
        this.album = album;
        this.albumId = albumId;
        this.title = title;


    }

}
