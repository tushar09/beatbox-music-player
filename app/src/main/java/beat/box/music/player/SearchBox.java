package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

;


public class SearchBox extends ActionBarActivity {

    EditText et;
    ActionBar ac;
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;

    ListView lvtitle;
    ListView lvalbum;
    ListView lvartist;

    public static Context context;
    public static ArrayList<Long> albumId;
    public static ArrayList artist;
    public static ArrayList title;
    public static ArrayList album;
    public static ArrayList songs;
    public static ArrayList songsPath;
    public static ArrayList songsDuration;
    Tracker t;
    AdView av;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_box);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("Search Box");
        t.send(new HitBuilders.AppViewBuilder().build());

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();

        av = (AdView) findViewById(R.id.adView);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        populate();
        ac = getSupportActionBar();
        ac.setDisplayHomeAsUpEnabled(false);
        ac.setDisplayShowCustomEnabled(true);
        ac.setDisplayShowTitleEnabled(false);
        ac.setDisplayHomeAsUpEnabled(true);
        ac.setIcon(R.drawable.ic_launcher);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.customactionbarforsearchbox, null);
        ac.setCustomView(v);
        ac.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));

        et = (EditText) findViewById(R.id.editText3);
        et.requestFocus();
        lvtitle = (ListView) findViewById(R.id.listView6);
        lvalbum = (ListView) findViewById(R.id.listView7);
        //lvartist = (ListView) findViewById(R.id.listView8);

        //et.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);


        TabHost th = (TabHost) findViewById(R.id.tabHost);
        th.setup();

        TabHost.TabSpec title1 = th.newTabSpec("Title");
        title1.setIndicator("Title");
        title1.setContent(R.id.tab1);
        th.addTab(title1);

        TabHost.TabSpec album1 = th.newTabSpec("Album");
        album1.setIndicator("Album");
        album1.setContent(R.id.tab2);
        th.addTab(album1);

//        TabHost.TabSpec artist1 = th.newTabSpec("Artist");
//        artist1.setIndicator("Artist");
//        artist1.setContent(R.id.tab3);
//        th.addTab(artist1);

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //Toast.makeText(SearchBox.this, s.toString(), Toast.LENGTH_SHORT).show();
                ArrayList titleInner = new ArrayList();
                ArrayList artistInner = new ArrayList();
                ArrayList albumInner = new ArrayList();
                ArrayList durationInner = new ArrayList();
                ArrayList pathInner = new ArrayList();
                ArrayList albumidInner = new ArrayList();

                ArrayList albumInnerForAlbum = new ArrayList();

                for(int t = 0; t < title.size(); t++){
                    if(("" + title.get(t)).toLowerCase().indexOf(s.toString().toLowerCase(Locale.US)) > -1){
                        titleInner.add(title.get(t));
                        artistInner.add(artist.get(t));
                        albumInner.add(album.get(t));
                        durationInner.add(songsDuration.get(t));
                        pathInner.add(songsPath.get(t));
                        albumidInner.add(albumId.get(t));
                    }
                }
                for(int t = 0; t < title.size(); t++){
                    if(("" + album.get(t)).toLowerCase().indexOf(s.toString().toLowerCase(Locale.US)) > -1){
                        //titleInner.add(title.get(t));
                        //artistInner.add(artist.get(t));
                        //if(!albumInnerForAlbum.contains(s.toString())){
                            albumInnerForAlbum.add(album.get(t));
                        //}

                        //durationInner.add(songsDuration.get(t));
                        //pathInner.add(songsPath.get(t));
                        //albumidInner.add(albumId.get(t));
                    }
                }
                //ArrayList<String> al = albumInnerForAlbum;
                // add elements to al, including duplicates
                Set<String> hs = new HashSet<>();
                hs.addAll(albumInnerForAlbum);
                albumInnerForAlbum.clear();
                albumInnerForAlbum.addAll(hs);
                if(s.toString().equals("") || s.toString().equals(" ") || s.toString().equals("  ") || s.toString().equals("   ") || s.toString().equals("    ") || s.toString().equals("     ") || s.toString().equals("      ") || s.toString().equals("       ") || s.toString().equals("         ") || s.toString().equals("          ") || s.toString().equals("           ")){
                    lvalbum.setVisibility(View.GONE);
                    lvtitle.setVisibility(View.GONE);
                }else{
                    lvalbum.setVisibility(View.VISIBLE);
                    lvtitle.setVisibility(View.VISIBLE);
                }
                lvtitle.setAdapter(new CustomListviewSearchboxtitle(SearchBox.this, titleInner, albumInner, artistInner, durationInner, pathInner, albumidInner, s.toString()));
                lvalbum.setAdapter(new CustomListviewSearchboxalbum(SearchBox.this, albumInnerForAlbum, s.toString()));
            }
        });

        //setTabColor(th);
    }

    private void populate() {
        ArrayList<Long> albumId = new ArrayList<Long>();
        ArrayList artist = new ArrayList();
        ArrayList title = new ArrayList();
        ArrayList album = new ArrayList();
        ArrayList songs = new ArrayList();
        ArrayList songsPath = new ArrayList();
        ArrayList songsDuration = new ArrayList();

        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        Cursor cursor;
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.YEAR,
                MediaStore.Audio.Media.COMPOSER
        };
        cursor = this.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);
        while (cursor.moveToNext()) {
            artist.add(cursor.getString(1));
            songsPath.add(cursor.getString(3));
            songs.add(cursor.getString(2));
            songsDuration.add(cursor.getString(5));
            album.add(cursor.getString(6));
            albumId.add(cursor.getLong(7));
            title.add(cursor.getString(2));
        }
        cursor.close();
        this.artist = artist;
        this.songsPath = songsPath;
        this.songs = songs;
        this.songsDuration = songsDuration;
        this.album = album;
        this.albumId = albumId;
        this.title = title;


    }

    public static void setTabColor(TabHost tabhost) {
        for(int i=0;i<tabhost.getTabWidget().getChildCount();i++) {
            tabhost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#FF0000")); //unselected
        }
        tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab()).setBackgroundColor(Color.parseColor("#0000FF")); // selected
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(SearchBox.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
        GoogleAnalytics.getInstance(SearchBox.this).reportActivityStart(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
