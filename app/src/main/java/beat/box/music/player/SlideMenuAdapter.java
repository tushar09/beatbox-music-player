package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tushar on 4/26/2015.
 */
public class SlideMenuAdapter extends BaseAdapter {
    public static final String qryTotalPlayTime = "totalPlayTime";
    public static final String qrySongname = "songName";
    public static final String qryCurrentPosition = "currentPosition";


    public static final String DEFAULT_SONGNAME = "N/A";
    public static final long DEFAULT_TOTALPLAYTIME = 0;
    public static final long DEFAULT_CURRENTPOSITION = 0;


    private Context context;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private static MainActivity f = new MainActivity();
    private ArrayList songs, artist, songduration, menu;


    private static LayoutInflater inflater = null;

    public SlideMenuAdapter(Context context, ArrayList menu){

        this.context = context;
        this.songs = menu;
//        this.songs = songs;
//        this.artist = artist;
//        this.songduration = songDuration;
//        sp = this.context.getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
//        editor = sp.edit();
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;

        //View rowView = convertView;
        if(convertView == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.drawer_list_item, null);
            holder = new Holder();
            holder.songTitle = (TextView) convertView.findViewById(R.id.title);
            holder.menu = (ImageView) convertView.findViewById(R.id.icon);
            //holder.songTitleArtist = (TextView) convertView.findViewById(R.id.songTitleArtist);
            //holder.songDuration = (TextView) convertView.findViewById(R.id.songDuration);
            //holder.menu = (ImageButton) convertView.findViewById(R.id.menu);
            convertView.setTag(holder);
        }else{
            //Log.i("TAG","inside");
            holder = (Holder) convertView.getTag();
            //rowView.setTag(holder);
        }
        holder.songTitle.setText("" + songs.get(position));
        if(songs.get(position).equals("Themes")){
            holder.menu.setBackgroundResource(R.drawable.theme);
        }
        //holder.songTitleArtist.setText("" + artist.get(position));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                f.fullPathList = new ArrayList(f.songsPath);
//                f.fullSongDurationList = new ArrayList(f.songsDuration);
//                f.fullTitleList = new ArrayList(f.songs);
//                f.fullArtistList = new ArrayList(f.artist);
//                f.fullalbumIdList = new ArrayList<Long>(f.albumId);
//
//                Intent palyer = new Intent(context, Player.class);
//                palyer.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                palyer.putExtra("canStart", true);
//                editor.putString("uri", "" + f.songsPath.get(position));
//                editor.putInt("songId", position);
//                Log.e("latest position checking", "" + position);
//                editor.commit();
//                context.startActivity(palyer);
            }
        });

//        holder.menu.setOnClickListener(new View.OnClickListener() {
//            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//            @Override
//            public void onClick(View v) {
//                PopupMenu pm = new PopupMenu(context, v);
//                MenuInflater mi = pm.getMenuInflater();
//                mi.inflate(R.menu.menu_songlist, pm.getMenu());
//                pm.show();
//                Toast.makeText(context, "asdfasdfasfd", Toast.LENGTH_SHORT).show();
//            }
//        });

        return convertView;
    }

//    @Override
//    public Object[] getSections() {
//        return sections;
//    }
//
//    @Override
//    public int getPositionForSection(int sectionIndex) {
//        return alphaIndexer.get(sections[sectionIndex]);
//    }
//
//    @Override
//    public int getSectionForPosition(int position) {
//        return 0;
//    }

    public static class Holder {
        TextView songTitle, songTitleArtist, songDuration;
        ImageView menu;
        int position;

    }




    public void filte(){
        //Log.e("safd","asdf");
    }
}
