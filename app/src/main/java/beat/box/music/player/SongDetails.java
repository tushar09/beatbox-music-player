package beat.box.music.player;

import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;


public class SongDetails extends ActionBarActivity {

    private static MainActivity m = new MainActivity();
    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    Tracker t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("Song Details");
        t.send(new HitBuilders.AppViewBuilder().build());

        setContentView(R.layout.activity_song_details);
        Bundle b = getIntent().getExtras();
        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
        getSupportActionBar().setTitle(b.getString("songName"));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageView iv = (ImageView) findViewById(R.id.imageView8);
        TextView name = (TextView) findViewById(R.id.textView24);
        name.setText(b.getString("songName"));
        long albumid = b.getLong("albumid");

        TextView composer = (TextView) findViewById(R.id.composer);
        composer.setText(b.getString("composer"));
        TextView year = (TextView) findViewById(R.id.year);
        year.setText(b.getString("year"));
        TextView artist = (TextView) findViewById(R.id.artist);
        artist.setText(b.getString("artist"));
        TextView album = (TextView) findViewById(R.id.album);
        album.setText(b.getString("album"));
        Picasso.with(this).load(getBitMapUri(albumid)).into(iv);
    }

    private Uri getBitMapUri(Long songId) {
        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, songId);
        return uri;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(SongDetails.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(SongDetails.this).reportActivityStart(this);
    }
}
