package beat.box.music.player;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.Spinner;

/**
 * Created by Tushar on 2/23/2015.
 */
public class SpinnerPreset extends Spinner {
    public SpinnerPreset(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSelectionByItemId(AdapterView<?> parent, long id){
        for (int i = 0; i < parent.getCount(); i++) {
            long itemIdAtPosition = parent.getItemIdAtPosition(i);
            if (itemIdAtPosition == id) {
                parent.setSelection(i);
                break;
            }
        }
    }


}
