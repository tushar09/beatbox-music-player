package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class Statistics extends ActionBarActivity {

    private static TextView totalPlayBackTimer;
    private static boolean handlerChk = false;
    private static Handler handler;
    private static Runnable updateTask;
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    private boolean bAnimation = true;
    Tracker t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("Library");
        t.send(new HitBuilders.AppViewBuilder().build());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f34335")));
        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
        totalPlayBackTimer = (TextView) findViewById(R.id.totalPlaybackTimer);
        handler = new Handler();
        updateTask = new Runnable() {
            @Override
            public void run() {
                update();
                handler.postDelayed(this, 1000);
            }
        };
        if(!handlerChk){
            handler.postDelayed(updateTask, 1000);
            handlerChk = true;
        }
    }

    private void update() {
        if (totalPlayBackTimer != null) {
            long hours, minutes, seconds;
            long totalTime = sp.getLong("totalTime", 0);
            hours = totalTime / 3600;
            minutes = (totalTime % 3600) / 60;
            seconds = totalTime % 60;
            String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
            //totalTimeInDatabase.setText(timeString);

            if(bAnimation){

                bAnimation = false;
                AlphaAnimation fadeIn = new AlphaAnimation( 0.0f , 1.0f );
                fadeIn.setDuration(2000);
                fadeIn.setFillAfter(true);
                totalPlayBackTimer.setText(timeString);
                totalPlayBackTimer.startAnimation(fadeIn);
            }else{
                totalPlayBackTimer.setText(timeString);
            }
        }
    }

    @Override
    protected void onStart() {
        GoogleAnalytics.getInstance(Statistics.this).reportActivityStart(this);
        bAnimation = true;
        handler.post(updateTask);
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(Statistics.this).reportActivityStop(this);
        handler.removeCallbacks(updateTask);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_statistics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
