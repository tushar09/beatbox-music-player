package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class Themes extends ActionBarActivity {

    public static TextView songInfo, dutarion, timer;
    public static SeekBar sk;
    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    public static ActionBar actionBar;
    public static RelativeLayout themeBase;
    public static Button invert;
    public boolean b = false, ecu = false;
    public static ImageView iv;
    public String currentColor = "f34335";
    public EgineBackground eg = new EgineBackground();
    public static ImageButton ib1, ib2, ib3, ib4, ib5, ib6, ib7, ib8, ib9, previous, play, next, suffel, repeat, applyTheme;
    Tracker t;
    AdView av;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themes);
        t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName("Themes");
        t.send(new HitBuilders.AppViewBuilder().build());

        av = (AdView) findViewById(R.id.adView2);
        //av.setAdUnitId("ca-app-pub-5018544316490781/6836553958");
        //av.setAdSize(AdSize.SMART_BANNER);
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)
                .build();
        av.loadAd(adRequest);

        sk = (SeekBar) findViewById(R.id.seekBar2);
        songInfo = (TextView) findViewById(R.id.textView9);
        dutarion = (TextView) findViewById(R.id.textView6);
        timer = (TextView) findViewById(R.id.textView8);

        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();

        actionBar = getSupportActionBar();
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //if(Build.VERSION.SDK_INT > 10){
        View v = inflator.inflate(R.layout.themeactionbar, null);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setIcon(R.drawable.ic_launcher);
        actionBar.setCustomView(v);
        actionBar.setDisplayHomeAsUpEnabled(true);
        themeBase = (RelativeLayout) findViewById(R.id.themeBase);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f34335")));
        iv = (ImageView) findViewById(R.id.imageView3);
        ib1 = (ImageButton) findViewById(R.id.imageButton2);
        ib2 = (ImageButton) findViewById(R.id.imageButton3);
        ib3 = (ImageButton) findViewById(R.id.imageButton4);
        ib4 = (ImageButton) findViewById(R.id.imageButton5);
        ib5 = (ImageButton) findViewById(R.id.imageButton6);
        ib6 = (ImageButton) findViewById(R.id.imageButton7);
        ib7 = (ImageButton) findViewById(R.id.imageButton8);
        ib8 = (ImageButton) findViewById(R.id.imageButton9);
        ib9 = (ImageButton) findViewById(R.id.imageButton25);
        previous = (ImageButton) findViewById(R.id.imageButton12);
        play = (ImageButton) findViewById(R.id.imageButton10);
        next = (ImageButton) findViewById(R.id.imageButton11);
        invert = (Button) findViewById(R.id.button);
        suffel = (ImageButton) findViewById(R.id.imageButton14);
        repeat = (ImageButton) findViewById(R.id.imageButton13);
        applyTheme = (ImageButton) findViewById(R.id.listViewSearch);
        applyTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apply();
            }
        });
        invert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (b) {
                    b = false;
                    ecu = false;
                } else {
                    b = true;
                    ecu = true;
                }
                invertColor();
            }
        });

        ib1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("f34335");
            }
        });
        ib2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("ff707a");

            }
        });
        ib3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("004dff");

            }
        });
        ib4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("d40025");

            }
        });
        ib5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("ff6b39");

            }
        });
        ib6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("2fb868");

            }
        });
        ib7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("009f6d");

            }
        });
        ib8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("ff4d00");

            }
        });
        ib9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyDemoTheme("33b5e5");

            }
        });


    }

    private void apply() {
        Toast.makeText(this, "Theme Applyed", Toast.LENGTH_LONG).show();
        editor.putString("color", currentColor);
        editor.putBoolean("inverted", b);
        editor.putBoolean("ecuSeekBar", ecu);
        editor.commit();
        if (b) {
            themeBase.setBackgroundColor(Color.parseColor("#99" + "ffffff"));

            if (currentColor.equals("009f6d")) {
                editor.putInt("notBg", R.drawable.deepgreennotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.deepgreen_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.deepgreen_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.deepgreenrepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.deepgreenrepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.deepgreensuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.deepgreen_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.deepgreen_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.deepgreenprevious);
                editor.putInt("play", R.drawable.deepgreenplay);
                editor.putInt("pause", R.drawable.deepgreenpause);
                editor.putInt("next", R.drawable.deepgreennext);
                editor.commit();

            } else if (currentColor.equals("ff4d00")) {
                editor.putInt("notBg", R.drawable.deeporangenotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.deeporange_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.deeporange_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.deeporangerepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.deeporangerepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.deeporangesuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.deeporange_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.deeporange_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.deeporangeprevious);
                editor.putInt("play", R.drawable.deeporangeplay);
                editor.putInt("pause", R.drawable.deeporangepause);
                editor.putInt("next", R.drawable.deeporangenext);
                editor.commit();
            } else if (currentColor.equals("2fb868")) {
                editor.putInt("notBg", R.drawable.greennotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.green_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.green_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.greenrepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.greenrepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.greensuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.green_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.green_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.greenprevious);
                editor.putInt("play", R.drawable.greenplay);
                editor.putInt("pause", R.drawable.greenpause);
                editor.putInt("next", R.drawable.greennext);
                editor.commit();
            } else if (currentColor.equals("d40025")) {
                editor.putInt("notBg", R.drawable.rednotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.red_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.red_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.redrepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.redrepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.redsuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.red_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.red_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.redprevious);
                editor.putInt("play", R.drawable.redplay);
                editor.putInt("pause", R.drawable.redpause);
                editor.putInt("next", R.drawable.rednext);
                editor.commit();
            } else if (currentColor.equals("ff6b39")) {
                editor.putInt("notBg", R.drawable.orangenotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.orenge_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.orenge_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.orangerepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.orangerepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.orangesuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.orenge_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.orenge_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.orangeprevious);
                editor.putInt("play", R.drawable.orangeplay);
                editor.putInt("pause", R.drawable.orangepause);
                editor.putInt("next", R.drawable.orangenext);
                editor.commit();
            } else if (currentColor.equals("004dff")) {
                editor.putInt("notBg", R.drawable.bluenotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.blue_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.blue_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.bluerepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.bluerepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.bluesuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.blue_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.blue_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.blueprevious);
                editor.putInt("play", R.drawable.blueplay);
                editor.putInt("pause", R.drawable.bluepause);
                editor.putInt("next", R.drawable.bluenext);
                editor.commit();
            } else if (currentColor.equals("33b5e5")) {
                editor.putInt("notBg", R.drawable.defaultnotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.default_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.default_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.defaultrepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.defaultrepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.defaultsuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.default_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.default_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.defaultprevious);
                editor.putInt("play", R.drawable.defaultplay);
                editor.putInt("pause", R.drawable.defaultpause);
                editor.putInt("next", R.drawable.defaultnext);
                editor.commit();
            } else if (currentColor.equals("ff707a")) {
                editor.putInt("notBg", R.drawable.semirednotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.semired_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.semired_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.semiredrepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.semiredrepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.semiredsuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.semired_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.semired_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.semiredprevious);
                editor.putInt("play", R.drawable.semiredplay);
                editor.putInt("pause", R.drawable.semiredpause);
                editor.putInt("next", R.drawable.semirednext);
                editor.commit();
            } else if (currentColor.equals("f34335")) {
                editor.putInt("notBg", R.drawable.updatednotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light);
                editor.putInt("repeatOne", R.drawable.updatedrepeatactivatedone);
                editor.putInt("repeatAll", R.drawable.updatedrepeatactivatedall);
                editor.putInt("suffelActivated", R.drawable.updatedsuffelactivated);
                editor.putInt("skPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light);
                editor.putInt("skThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light);
                editor.putInt("previous", R.drawable.updatedprevious);
                editor.putInt("play", R.drawable.updatedplay);
                editor.putInt("pause", R.drawable.updatedpause);
                editor.putInt("next", R.drawable.updatednext);
                editor.commit();
            }

        } else {
            if (currentColor.equals("009f6d")) {
                editor.putInt("notBg", R.drawable.deepgreennotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.deepgreen_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.deepgreen_scrubber_control_selector_holo_light);

                editor.commit();

            } else if (currentColor.equals("ff4d00")) {
                editor.putInt("notBg", R.drawable.deeporangenotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.deeporange_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.deeporange_scrubber_control_selector_holo_light);

                editor.commit();
            } else if (currentColor.equals("2fb868")) {
                editor.putInt("notBg", R.drawable.greennotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.green_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.green_scrubber_control_selector_holo_light);

                editor.commit();
            } else if (currentColor.equals("d40025")) {
                editor.putInt("notBg", R.drawable.rednotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.red_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.red_scrubber_control_selector_holo_light);

                editor.commit();
            } else if (currentColor.equals("ff6b39")) {
                editor.putInt("notBg", R.drawable.orangenotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.orenge_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.orenge_scrubber_control_selector_holo_light);

                editor.commit();
            } else if (currentColor.equals("004dff")) {
                editor.putInt("notBg", R.drawable.bluenotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.blue_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.blue_scrubber_control_selector_holo_light);

                editor.commit();
            } else if (currentColor.equals("33b5e5")) {
                editor.putInt("notBg", R.drawable.defaultnotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.default_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.default_scrubber_control_selector_holo_light);

                editor.commit();
            } else if (currentColor.equals("ff707a")) {
                editor.putInt("notBg", R.drawable.semirednotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.semired_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.semired_scrubber_control_selector_holo_light);

                editor.commit();
            } else if (currentColor.equals("f34335")) {
                editor.putInt("notBg", R.drawable.updatednotbg);
                editor.putInt("ecuskPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light);
                editor.putInt("ecuskThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light);

                editor.commit();
            }
            editor.putInt("repeatOne", R.drawable.repeatactivatedone);
            editor.putInt("repeatAll", R.drawable.repeatactivatedall);
            editor.putInt("suffelActivated", R.drawable.suffleactivated);
            editor.putInt("skPrgressDrable", R.drawable.red_scrubber_progress);
            editor.putInt("skThumb", R.drawable.red_scrubber_control);
            editor.putInt("previous", R.drawable.previous);
            editor.putInt("play", R.drawable.play);
            editor.putInt("pause", R.drawable.pause);
            editor.putInt("next", R.drawable.next);
            editor.commit();
        }

        if(eg.mp != null){
            if(eg.mp.isPlaying()){
                eg.notificationColorChange();
            }
        }

    }

    private void invertColor() {
        if (b) {
            themeBase.setBackgroundColor(Color.parseColor("#99" + "ffffff"));
            Rect bound = sk.getProgressDrawable().getBounds();
            if (currentColor.equals("009f6d")) {
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.deepgreen_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.deepgreen_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);
                previous.setBackgroundResource(R.drawable.deepgreenprevious);
                play.setBackgroundResource(R.drawable.deepgreenplay);
                next.setBackgroundResource(R.drawable.deepgreennext);
                suffel.setBackgroundResource(R.drawable.deepgreensuffelactivated);
                repeat.setBackgroundResource(R.drawable.deepgreenrepeatactivatedall);
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));

//                editor.putInt("repeatOne", R.drawable.deepgreenrepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.deepgreenrepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.deepgreensuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.deepgreen_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.deepgreen_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.deepgreenprevious);
//                editor.putInt("play", R.drawable.deepgreenplay);
//                editor.putInt("pause", R.drawable.deepgreenpause);
//                editor.putInt("next", R.drawable.deepgreennext);
//                editor.commit();

            } else if (currentColor.equals("ff4d00")) {
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));
                repeat.setBackgroundResource(R.drawable.deeporangerepeatactivatedall);
                suffel.setBackgroundResource(R.drawable.deeporangesuffelactivated);
                next.setBackgroundResource(R.drawable.deeporangenext);
                play.setBackgroundResource(R.drawable.deeporangeplay);
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.deeporange_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.deeporange_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);
                previous.setBackgroundResource(R.drawable.deeporangeprevious);

//                editor.putInt("repeatOne", R.drawable.deeporangerepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.deeporangerepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.deeporangesuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.deeporange_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.deeporange_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.deeporangeprevious);
//                editor.putInt("play", R.drawable.deeporangeplay);
//                editor.putInt("pause", R.drawable.deeporangepause);
//                editor.putInt("next", R.drawable.deeporangenext);
//                editor.commit();
            } else if (currentColor.equals("2fb868")) {
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));
                repeat.setBackgroundResource(R.drawable.greenrepeatactivatedall);
                suffel.setBackgroundResource(R.drawable.greensuffelactivated);
                next.setBackgroundResource(R.drawable.greennext);
                play.setBackgroundResource(R.drawable.greenplay);
                previous.setBackgroundResource(R.drawable.greenprevious);
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.green_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.green_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);

//                editor.putInt("repeatOne", R.drawable.greenrepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.greenrepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.greensuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.green_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.green_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.greenprevious);
//                editor.putInt("play", R.drawable.greenplay);
//                editor.putInt("pause", R.drawable.greenpause);
//                editor.putInt("next", R.drawable.greennext);
//                editor.commit();
            } else if (currentColor.equals("d40025")) {
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));
                repeat.setBackgroundResource(R.drawable.redrepeatactivatedall);
                suffel.setBackgroundResource(R.drawable.redsuffelactivated);
                next.setBackgroundResource(R.drawable.rednext);
                play.setBackgroundResource(R.drawable.redplay);
                previous.setBackgroundResource(R.drawable.redprevious);
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.red_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.red_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);

//                editor.putInt("repeatOne", R.drawable.redrepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.redrepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.redsuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.red_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.red_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.redprevious);
//                editor.putInt("play", R.drawable.redplay);
//                editor.putInt("pause", R.drawable.redpause);
//                editor.putInt("next", R.drawable.rednext);
//                editor.commit();
            } else if (currentColor.equals("ff6b39")) {
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));
                repeat.setBackgroundResource(R.drawable.orangerepeatactivatedall);
                suffel.setBackgroundResource(R.drawable.orangesuffelactivated);
                next.setBackgroundResource(R.drawable.orangenext);
                play.setBackgroundResource(R.drawable.orangeplay);
                previous.setBackgroundResource(R.drawable.orangeprevious);
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.orenge_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.orenge_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);

//                editor.putInt("repeatOne", R.drawable.orangerepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.orangerepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.orangesuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.orenge_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.orenge_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.orangeprevious);
//                editor.putInt("play", R.drawable.orangeplay);
//                editor.putInt("pause", R.drawable.orangepause);
//                editor.putInt("next", R.drawable.orangenext);
//                editor.commit();
            } else if (currentColor.equals("004dff")) {
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));
                repeat.setBackgroundResource(R.drawable.bluerepeatactivatedall);
                suffel.setBackgroundResource(R.drawable.bluesuffelactivated);
                next.setBackgroundResource(R.drawable.bluenext);
                play.setBackgroundResource(R.drawable.blueplay);
                previous.setBackgroundResource(R.drawable.blueprevious);
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.blue_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.blue_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);

//                editor.putInt("repeatOne", R.drawable.bluerepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.bluerepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.bluesuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.blue_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.blue_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.blueprevious);
//                editor.putInt("play", R.drawable.blueplay);
//                editor.putInt("pause", R.drawable.bluepause);
//                editor.putInt("next", R.drawable.bluenext);
//                editor.commit();
            } else if (currentColor.equals("33b5e5")) {
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));
                repeat.setBackgroundResource(R.drawable.defaultrepeatactivatedall);
                suffel.setBackgroundResource(R.drawable.defaultsuffelactivated);
                next.setBackgroundResource(R.drawable.defaultnext);
                play.setBackgroundResource(R.drawable.defaultplay);
                previous.setBackgroundResource(R.drawable.defaultprevious);
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.default_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.default_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);

//                editor.putInt("repeatOne", R.drawable.defaultrepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.defaultrepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.defaultsuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.default_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.default_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.defaultprevious);
//                editor.putInt("play", R.drawable.defaultplay);
//                editor.putInt("pause", R.drawable.defaultpause);
//                editor.putInt("next", R.drawable.defaultnext);
//                editor.commit();
            } else if (currentColor.equals("ff707a")) {
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));
                repeat.setBackgroundResource(R.drawable.semiredrepeatactivatedall);
                suffel.setBackgroundResource(R.drawable.semiredsuffelactivated);
                next.setBackgroundResource(R.drawable.semirednext);
                play.setBackgroundResource(R.drawable.semiredplay);
                previous.setBackgroundResource(R.drawable.semiredprevious);
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.semired_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.semired_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);

//                editor.putInt("repeatOne", R.drawable.semiredrepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.semiredrepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.semiredsuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.semired_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.semired_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.semiredprevious);
//                editor.putInt("play", R.drawable.semiredplay);
//                editor.putInt("pause", R.drawable.semiredpause);
//                editor.putInt("next", R.drawable.semirednext);
//                editor.commit();
            } else if (currentColor.equals("f34335")) {
                songInfo.setTextColor(Color.parseColor("#99" + currentColor));
                dutarion.setTextColor(Color.parseColor("#99" + currentColor));
                timer.setTextColor(Color.parseColor("#99" + currentColor));
                repeat.setBackgroundResource(R.drawable.updatedrepeatactivatedall);
                suffel.setBackgroundResource(R.drawable.updatedsuffelactivated);
                next.setBackgroundResource(R.drawable.updatednext);
                play.setBackgroundResource(R.drawable.updatedplay);
                previous.setBackgroundResource(R.drawable.updatedprevious);
                sk.setProgressDrawable(getResources().getDrawable(R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light));
                sk.setThumb(getResources().getDrawable(R.drawable.updatedseekbars_scrubber_control_selector_holo_light));
                sk.getProgressDrawable().setBounds(bound);

//                editor.putInt("repeatOne", R.drawable.updatedrepeatactivatedone);
//                editor.putInt("repeatAll", R.drawable.updatedrepeatactivatedall);
//                editor.putInt("suffelActivated", R.drawable.updatedsuffelactivated);
//                editor.putInt("skPrgressDrable", R.drawable.updatedseekbars_scrubber_progress_horizontal_holo_light);
//                editor.putInt("skThumb", R.drawable.updatedseekbars_scrubber_control_selector_holo_light);
//                editor.putInt("previous", R.drawable.updatedprevious);
//                editor.putInt("play", R.drawable.updatedplay);
//                editor.putInt("pause", R.drawable.updatedpause);
//                editor.putInt("next", R.drawable.updatednext);
//                editor.commit();
            }
//        sk.setProgressDrawable(getResources().getDrawable(R.drawable.blue_scrubber_progress_horizontal_holo_light));
//        sk.setThumb(getResources().getDrawable(R.drawable.blue_scrubber_control_selector_holo_light));
//        sk.getProgressDrawable().setBounds(bound);
            songInfo.setTextColor(Color.parseColor("#" + currentColor));
            timer.setTextColor(Color.parseColor("#" + currentColor));
            dutarion.setTextColor(Color.parseColor("#" + currentColor));
        } else {
            repeat.setBackgroundResource(R.drawable.repeatactivatedall);
            suffel.setBackgroundResource(R.drawable.suffleactivated);
            next.setBackgroundResource(R.drawable.next);
            play.setBackgroundResource(R.drawable.play);
            previous.setBackgroundResource(R.drawable.previous);
            themeBase.setBackgroundColor(Color.parseColor("#99" + currentColor));
            Rect bound = sk.getProgressDrawable().getBounds();
            songInfo.setTextColor(Color.parseColor("#" + "ffffff"));
            timer.setTextColor(Color.parseColor("#" + "ffffff"));
            dutarion.setTextColor(Color.parseColor("#" + "ffffff"));
            sk.setProgressDrawable(getResources().getDrawable(R.drawable.red_scrubber_progress));
            sk.setThumb(getResources().getDrawable(R.drawable.red_scrubber_control));
            sk.getProgressDrawable().setBounds(bound);

//            editor.putInt("repeatOne", R.drawable.repeatactivatedone);
//            editor.putInt("repeatAll", R.drawable.repeatactivatedall);
//            editor.putInt("suffelActivated", R.drawable.suffleactivated);
//            editor.putInt("skPrgressDrable", R.drawable.red_scrubber_progress);
//            editor.putInt("skThumb", R.drawable.red_scrubber_control);
//            editor.putInt("previous", R.drawable.previous);
//            editor.putInt("play", R.drawable.play);
//            editor.putInt("pause", R.drawable.pause);
//            editor.putInt("next", R.drawable.next);
//            editor.commit();
        }

    }

    private void applyDemoTheme(String color) {
        currentColor = color;
        b = false;
        invertColor();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + color)));
        invert.setBackgroundColor(Color.parseColor("#" + color));
        themeBase.setBackgroundColor(Color.parseColor("#99" + color));
        iv.setBackgroundColor(Color.parseColor("#" + color));

    }


    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(Themes.this).reportActivityStop(this);
    }
    @Override
    protected void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            av.setVisibility(View.GONE);
        }
        super.onStart();
        GoogleAnalytics.getInstance(Themes.this).reportActivityStart(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
