package beat.box.music.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;


public class sleep extends ActionBarActivity {

    public static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    private static TextView tv;
    private static Switch sw;
    private static SeekBar sb;
    private String format = "";
    public static ActionBar actionBar;
    public static LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep);
        sp = getSharedPreferences("setPrefernces", Context.MODE_PRIVATE);
        editor = sp.edit();
        actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
        actionBar.setElevation(0);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.sleepactionbar, null);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setIcon(R.drawable.ic_launcher);
        actionBar.setCustomView(v);
        sw = (Switch) findViewById(R.id.switch2);


        tv = (TextView) findViewById(R.id.textView21);
        tv.setTextColor(Color.parseColor("#" + sp.getString("color", "f34335")));
        if(!sw.isChecked()){
            tv.setText("Please Turn On Sleep Mode To Set Time");
        }

        sb = (SeekBar) findViewById(R.id.seekBar11);
        sb.setProgressDrawable(getResources().getDrawable(sp.getInt("ecuskPrgressDrable", R.drawable.red_scrubber_progress)));
        sb.setThumb(getResources().getDrawable(sp.getInt("ecuskThumb", R.drawable.red_scrubber_progress)));
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tv.setText(progress + " Minutes");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ll = (LinearLayout) findViewById(R.id.linearLayout);
        ll.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + sp.getString("color", "f34335"))));
    }


    public void checked(View v){
        if (sw.isChecked()){
            //Toast.makeText(this, "ghjghjg", Toast.LENGTH_LONG).show();
            editor.putBoolean("sleepActivated", true);
            editor.commit();
            sb.setEnabled(true);
            tv.setText(sb.getProgress() + " Minute");
        }else{
            editor.putBoolean("sleepActivated", false);
            editor.commit();
            sb.setEnabled(false);
            tv.setText("Please Turn On Sleep Mode To Set Time");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sleep, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(sw.isChecked()){
            editor.putBoolean("sleepActivated", true);
            editor.putInt("sleepTime", sb.getProgress() * 60);
            //editor.putInt("startCount", 0);
            editor.commit();
        }else{
            editor.putBoolean("sleepActivated", false);
            editor.commit();
        }

        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        sw.setChecked(sp.getBoolean("sleepActivated", false));
        if(!sp.getBoolean("sleepActivated", false)){
            sb.setEnabled(false);
        }
        super.onStart();
    }
}
